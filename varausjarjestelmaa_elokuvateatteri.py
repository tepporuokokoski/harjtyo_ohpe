"""Harjoitustyö_ohjelmaa elokuvateatterin varausjärjestelmäohjelmasta
(mm. maksuasioita ja kirjautumista järjestelmään ei huomioituna ja tehdyssä ylläpitäjä-käyttöliittymässä
kohtia 14-16 ja asiakas-käyttöliittymässä varauksen muuttamista ja kohtia 7-8 ei ole ohjelmoituna, mutta mahd.ideoina)"""

def mainvalikko_yllapitaja():
    """Näyttää varausjärjestelmän ylläpitäjä-käyttöliittymän"""
    
    while True:
        try:
            print("Valitse toiminto: ")
            print("1. Näytä ohjelmaan tallennetut elokuvat")
            print("2. Näytä viikko-ohjelma")
            print("3. Näytä päivän tämän hetkinen varaustilanne") 
            print("4. Näytä istumapaikkojen varaustilanne haetussa näytöksessä")
            print("5. Hae varauksen tiedot")
            print("6. Näytä salitiedot")
            print("7. Tee viikko-ohjelmaa, lisää näytös")
            print("8. Poista viikko-ohjelmasta näytös (laadinnassa olevasta)")
            print("9. Näytä karkeasuunnitelmaa")
            print("10. Tee karkeasuunnitelmaa (esim. viikko-ohjelmasta 1kk eteenpäin)")
            print("11. Lisää/tallenna elokuvia ohjelmaan tai muokkaa elokuvatietoja")
            print("12. Poista ohjelmaan tallennettuja elokuvia")
            print("13. Lisää tai muokkaa salitietoja")
            print("14. Hae elokuvien aiempia varauslukumääriä (nykyhetki - taaksepäin 2vko / 1kk / elokuvan ohjelmistoon tulosta lähtien)") # voisi jatkaa, ei ohjelmoitu
            print("15. Katso saatuja palautteita") # voisi jatkaa, olisi hyvä, ei ohjelmoitu
            print("16. Lisää ideoita kehitykseen") # voisi jatkaa, ei ohjelmoitu
            print("0. Poistu")

            print("")
            valinta = int(input("Valintasi: "))

            if valinta == 1: 
                nayta_tallennetut_elokuvat('elokuva', 'genre', 'kesto') 
            elif valinta == 2: # voisi olla esim. 1-2 vahvistettua viikko-ohjelmaa kerrallaan voimassa
                nayta_viikko_ohjelma('viikko_nro', 'pvm', 'viikonpaiva', 'tunnistenro', 'elokuva', 'salinro', 'aloitusaika', 'paattymisaika')
            elif valinta == 3: # koontina haetun päivän näytöksiin näytös- ja salitiedot sekä tämän hetkiset varausten lukumäärät/maksimimäärät
                paivan_varaustilanne() # pvm, viikonpaiva, naytos_tunnistenro, aloitusaika-päättymisaika, salinro, varauslukumaara/maksimimäärä, elokuva
            elif valinta == 4: # näytöksen haku sekä kyseiseen näytökseen vapaiden istumapaikkojen näyttäminen
                naytoksen_varaustilanne('pvm', 'naytos_tunnistenro', 'elokuva', 'aloitusaika', 'salinro', 'varaus_istumapaikka', 'varaus_tunnistenro')
            elif valinta == 5: # tunnistenumerolla varauksen tietojen haku 
                varaustiedot_tunnistenro() # 'naytos_pvm', 'viikonpaiva', 'naytos_tunnistenro', 'elokuva', 'aloitusaika', 'paattymisaika' 'salinro', 'varattu_istumapaikka', 'varaus_tunnistenro'
            elif valinta == 6: 
                nayta_salitiedot('salinro', 'istumapaikkalkm', 'rivimaara', 'istumapaikkoja_per_rivi', 'muita_salitietoja')
            elif valinta == 7: # lisää viikko_ohjelmaan näytös, voisi olla esim. 1-2 vahvistettua viikko-ohjelmaa kerrallaan voimassa
                tee_viikko_ohjelmaa('viikko_nro', 'pvm', 'viikonpaiva', 'tunnistenro', 'elokuva', 'salinro', 'aloitusaika', 'paattymisaika')
            elif valinta == 8: # poista laadinnassa olevasta viikko-ohjelmasta näytös
                poista_viikko_ohjelmasta('viikko_nro', 'pvm', 'viikonpaiva', 'tunnistenro', 'elokuva', 'salinro', 'aloitusaika', 'paattymisaika')
            elif valinta == 9: 
                nayta_karkeasuunnitelmaa('aloitusviikko', 'paatosviikko', 'elokuva', 'genre', 'kesto')
            elif valinta == 10: # elokuvien alustavia ajoituksia viikko-ohjelmistoihin (esim. viikko-ohjelmasta 1kk eteenpäin)
                tee_karkeasuunnitelmaa('elokuva', 'genre', 'kesto', 'aloitusviikko', 'paatosviikko')
            elif valinta == 11: # ohjelmatiedostoon elokuvien ja -tietojen lisääminen tai näiden muokkaaminen
                lisaa_elokuva_taimuokkaa('elokuva', 'genre', 'kesto')
            elif valinta == 12: # poista elokuva ohjelmatiedostosta
                poista_elokuva('elokuva', 'genre', 'kesto')
            elif valinta == 13: 
                lisaa_muokkaa_salitietoja('salinro', 'istumapaikkalkm', 'rivimaara', 'istumapaikkoja_per_rivi', 'muita_salitietoja')
            elif valinta == 14: # voisi jatkaa, ei ohjelmoitu, voisi hakea elokuvien varauslukumäärä-koosteita esim. edellinen päivä ja 2 viikkoa taaksepäin / 1kk taaksepäin / elokuvan ohjelmistoon tulosta lähtien
                hae_elokuvan_aiempia_varauslkm()
            elif valinta == 15: # olisi hyvä, ei ohjelmoitu, näyttäisi palautteet, joita asiakkaat voisivat antaa
                palautteet()
            elif valinta == 16: # voisi jatkaa, ei ohjelmoitu, voisi tallentaa esiin tulleita/mieleen tulleita ideoita ohjelmaan ja näin "ideapankkia" kehittämiseen
                lisaa_ideoita()
            elif valinta == 0: 
                break
            elif valinta not in range(0, 17):
                print(" Valinta pitäisi antaa numerona väliltä 0-16 (alla olevista)")
            print("")
        except ValueError:
            virheilmoitus("Valinta pitäisi antaa numerona väliltä 0-16 (alla olevista)")
            print("")

def mainvalikko_asiakkaalle():
    """Näyttää varausjärjestelmän asiakas-käyttöliittymän"""

    while True:
        try:
            print("Valitse toiminto: ")
            print("1. Katso elokuvien viikko-ohjelma")
            print("2. Katso tämän päivän myöhemmät näytökset")
            print("3. Näytä istumapaikkojen varaustilanne haetussa näytöksessä")
            print("4. Näytä salitiedot")
            print("5. Tee elokuvanäytökseen varaus (päivämäärä, elokuva, näytösaika, sali, istumapaikka)")
            print("6. Näytä varauksen tiedot tai muokkaa varausta") # voisi jatkaa, mahd. varausmuutosten tekemistä ei ohjelmoitu
            print("7. Anna elokuvalle arvostelu") # voisi jatkaa, ei ohjelmoitu
            print("8. Anna palautetta (kuinka hyvin onnistuimme?)") # voisi jatkaa, voisi olla hyvä, ei ohjelmoitu
            print("0. Poistu")

            print("")
            valinta = int(input("Valintasi: "))

            if valinta == 1: 
                nayta_viikko_ohjelma('viikko_nro', 'pvm', 'viikonpaiva', 'tunnistenro', 'elokuva', 'salinro', 'aloitusaika', 'paattymisaika')
            elif valinta == 2:
                paivan_myohemmat_naytokset('pvm', 'elokuva', 'aloitusaika', 'paattymisaika', 'salinro')
            elif valinta == 3:
                naytoksen_varaustilanne('pvm', 'naytos_tunnistenro', 'elokuva', 'aloitusaika', 'salinro', 'varaus_istumapaikka', 'varaus_tunnistenro')        
            elif valinta == 4:
                nayta_salitiedot('salinro', 'istumapaikkalkm', 'rivimaara', 'istumapaikkoja_per_rivi', 'muita_salitietoja')
            elif valinta == 5:
                varauksen_tekeminen('varaus_pvm', 'varaus_elokuva', 'varaus_aloitusaika', 'varaus_salinro', 'varaus_istumapaikka', 'varaus_tunnistenro')
            elif valinta == 6: # voisi jatkaa, ei ohjelmoitu, (hyvä olisi olla sisäänkirjautumisen kautta)
                varaustiedot_mahd_muokkaa()
            elif valinta == 7: # voisi jatkaa, ei ohjelmoitu
                anna_elokuva_arvostelu()
            elif valinta == 8: # voisi jatkaa, olisi hyvä, ei ohjelmoitu
                palautteen_antaminen()
            elif valinta == 0:
                break
            elif valinta not in range(0, 9):
                print(" Valinta pitäisi antaa numerona väliltä 0-8 (alla olevista)")
            print("")
        except ValueError:
            virheilmoitus("Valinta pitäisi antaa numerona väliltä 0-8 (alla olevista)")
            print("")
            

def elokuvat_txt_tarkistus_paivitys(): 
# käytetään aliohjelmana (modulaarisena), tarkistetaan elokuvat.txt tiedosto (jos tietoja ladattaisiin ohjelmaan tiedoston manuaalisella muokkauksella)
    from datetime import datetime
    elokuvat = []
    e_nimi = []
    saman_niminen = []
    try:
        with open("elokuvat.txt", "r") as elokuvat_tiedosto: # elokuva, genre, kesto
            for rivi in elokuvat_tiedosto:
                if rivi == "\n" or rivi in elokuvat: # jos tiedostoa elokuvat.txt on muokattu manuaalisesti, niin mahd. tyhjien rivien poisto sekä mahd. samojen elokuvatieto-rivien poisto
                    del rivi
                elif rivi.split(", ")[0] in e_nimi: # jos tiedostoa elokuvat.txt on muokattu manuaalisesti ja samannimisestä elokuvasta enemmän kuin 1 rivi, niin jätetään vain 1 rivi
                    saman_niminen.append(rivi.split(", ")[0])
                    del rivi
                else:
                    elokuvatiedot_osat = rivi.strip().split(", ")
                    elokuvatiedot_osat[0] = elokuvatiedot_osat[0].strip().lower() # muutetaan tiedoston riveiltä mahd. isot kirjaimet pieniksi ja poistetaan mahd. välilyönnit
                    elokuvatiedot_osat[1] = elokuvatiedot_osat[1].strip().lower()
                    elokuvatiedot_osat[2] = elokuvatiedot_osat[2].strip()
                    elokuvatiedot_osat[2] = datetime.strptime(elokuvatiedot_osat[2], '%H:%M') # muutetaan tiedoston riveiltä mahd. ei-datetime muotoiset kestot datetime-muotoon
                    elokuvatiedot_osat[2] = elokuvatiedot_osat[2].strftime('%H:%M')
                    elokuvat.append(", ".join(elokuvatiedot_osat) + "\n")
                    e_nimi.append(elokuvatiedot_osat[0])
            elokuvat.sort(key=lambda elokuvatiedot_osat: elokuvatiedot_osat[0]) # muutetaan aakkosjärjestykseen elokuvan nimen perusteella
            with open("elokuvat.txt", "w") as elokuvat_tiedosto:
                elokuvat_tiedosto.writelines(elokuvat) # tallennetaan elokuvalista mahd. tehdyillä muutoksilla
            while True:
                if saman_niminen == []: # eli tyhjä
                    break         
                else:
                    print(f" Automaattisena päivityksenä: Tarkista elokuvasta/elokuvista {saman_niminen} tallentuneet tiedot ja tarvittaessa muokkaa, tiedostoon elokuvat.txt jätettiin 1 rivi per elokuva, että ohjelma toimisi oikein")
                    print(" (tiedostoon elokuvat.txt oli epähuomiossa ilmeisesti manuaalisesti mennyt samasta elokuvasta eri rivejä)")
                    print("")
                    break
    except ValueError:
        print("")
        virheilmoitus("Tiedostossa elokuvat.txt kaikki rivit pitäisi olla oikeassa muodossa (elokuva, genre, kesto) (jos muokattu manuaalisesti) (ValueError)")
        virheilmoitus("Mahdollisena ehdotuksena: muokkaa/poista virheellinen rivi manuaalisesti elokuvat.txt tiedostosta tai ota tiedot talteen, tyhjennä elokuvat.txt tiedosto, lisää ohjelmalla esim. 2 elokuvaa ja katso tämän jälkeen elokuvat.txt tiedostoon tallentunut muoto (ja tallenna)")
        print("")
        mainvalikko_yllapitaja()
    except IndexError:
        print("")
        virheilmoitus("Tiedostossa elokuvat.txt kaikki rivit pitäisi olla oikeassa muodossa (elokuva, genre, kesto) (jos muokattu manuaalisesti) (IndexError)")
        virheilmoitus("Mahdollisena ehdotuksena: muokkaa/poista virheellinen rivi manuaalisesti elokuvat.txt tiedostosta tai ota tiedot talteen, tyhjennä elokuvat.txt tiedosto, lisää ohjelmalla esim. 2 elokuvaa ja katso tämän jälkeen elokuvat.txt tiedostoon tallentunut muoto (ja tallenna)")    
        print("")
        mainvalikko_yllapitaja()
        
def karkeasuunnitelmaa_tarkistus_paivitys():   
    # käytetään aliohjelmana (modulaarisena), tarkistetaan karkeasuunnitelmaa.txt tiedosto ja mahd. päivitetään (mm. jos mahd. manuaalisesti lisättyihin olisi jäänyt ylimääräisiä rivejä, välilyöntejä tai samasta elokuvasta eri rivejä)      
    from datetime import datetime
    elokuvat_karkeasuunnitelmaa = []
    k_nimi = []
    saman_niminen = []
    try:
        with open("karkeasuunnitelmaa.txt", "r") as karkeasuunnitelmaa_tiedosto: # aloitusviikko, paatosviikko, elokuva, genre, kesto
            for rivi in karkeasuunnitelmaa_tiedosto:
                if rivi == "\n" or rivi in elokuvat_karkeasuunnitelmaa: # jos tiedostoa karkeasuunnitelmaa.txt on muokattu manuaalisesti, niin mahd. tyhjien rivien poisto sekä mahd. samojen karkeasuunnitelma-rivien poisto
                    del rivi
                elif rivi.split(", ")[2] in k_nimi: # jos tiedostoa karkeasuunnitelmaa.txt on muokattu manuaalisesti ja samannimisestä elokuvasta enemmän kuin 1 rivi, niin jätetään vain 1 rivi
                    saman_niminen.append(rivi.split(", ")[2])
                    del rivi
                else:
                    karkeasuunnitelmaa_osat = rivi.strip().split(", ")
                    karkeasuunnitelmaa_osat[0] = karkeasuunnitelmaa_osat[0].strip()
                    karkeasuunnitelmaa_osat[1] = karkeasuunnitelmaa_osat[1].strip()
                    karkeasuunnitelmaa_osat[2] = karkeasuunnitelmaa_osat[2].strip().lower() # muutetaan tiedoston riveiltä mahd. isot kirjaimet pieniksi ja poistetaan mahd. välilyönnit
                    karkeasuunnitelmaa_osat[3] = karkeasuunnitelmaa_osat[3].strip().lower()
                    karkeasuunnitelmaa_osat[4] = karkeasuunnitelmaa_osat[4].strip()
                    karkeasuunnitelmaa_osat[4] = datetime.strptime(karkeasuunnitelmaa_osat[4], '%H:%M') # muutetaan tiedoston riveiltä mahd. ei-datetime muotoiset kestot datetime-muotoon
                    karkeasuunnitelmaa_osat[4] = karkeasuunnitelmaa_osat[4].strftime('%H:%M')
                    elokuvat_karkeasuunnitelmaa.append(", ".join(karkeasuunnitelmaa_osat) + "\n")
                    k_nimi.append(karkeasuunnitelmaa_osat[2])
            elokuvat_karkeasuunnitelmaa.sort(key=lambda karkeasuunnitelmaa_osat: karkeasuunnitelmaa_osat[0]) # muutetaan järjestykseen aloitusviikon perusteella
        with open("karkeasuunnitelmaa.txt", "w") as karkeasuunnitelmaa_tiedosto:
            karkeasuunnitelmaa_tiedosto.writelines(elokuvat_karkeasuunnitelmaa) # tallennetaan elokuvalista mahd. tehdyillä muutoksilla
        while True:
            if saman_niminen == []: # eli tyhjä
                break         
            else:
                print(f" Automaattisena päivityksenä: Tarkista karkeasuunnitelmaan elokuvasta/elokuvista {saman_niminen} tallentuneet tiedot ja tarvittaessa muokkaa, tiedostoon karkeasuunnitelmaa.txt jätettiin 1 rivi per elokuva, että ohjelma toimisi oikein")
                print(" (tiedostoon karkeasuunnitelmaa.txt oli epähuomiossa ilmeisesti manuaalisesti mennyt samasta elokuvasta eri rivejä)")
                print("")
                break
    except ValueError:
        print("")
        virheilmoitus("Tiedostossa karkeasuunnitelmaa.txt kaikki rivit pitäisi olla oikeassa muodossa (aloitusviikko, päätösviikko, elokuva, genre, kesto) (jos muokattu manuaalisesti) (ValueError)")
        virheilmoitus("Mahdollisena ehdotuksena: muokkaa/poista virheellinen rivi manuaalisesti karkeasuunnitelmaa.txt tiedostosta tai ota tiedot talteen, tyhjennä karkeasuunnitelmaa.txt tiedosto, tee ohjelmalla esim. 2 elokuvaan karkeasuunnitelmaa ja katso tämän jälkeen karkeasuunnitelmaa.txt tiedostoon tallentunut muoto (ja tallenna)")
        print("")
        mainvalikko_yllapitaja()
    except IndexError:
        print("")
        virheilmoitus("Tiedostossa karkeasuunnitelmaa.txt kaikki rivit pitäisi olla oikeassa muodossa (aloitusviikko, päätösviikko, elokuva, genre, kesto) (jos muokattu manuaalisesti) (IndexError)")
        virheilmoitus("Mahdollisena ehdotuksena: muokkaa/poista virheellinen rivi manuaalisesti karkeasuunnitelmaa.txt tiedostosta tai ota tiedot talteen, tyhjennä karkeasuunnitelmaa.txt tiedosto, tee ohjelmalla esim. 2 elokuvaan karkeasuunnitelmaa ja katso tämän jälkeen karkeasuunnitelmaa.txt tiedostoon tallentunut muoto (ja tallenna)")
        print("")
        mainvalikko_yllapitaja()
    
def elokuvat_autom_lisayspaivitys(): # (käytetään modulaarisena aliohjelmana)    
    # tehdään automaattisesti elokuvat.txt päivitys, jos manuaalisesti oltaisiin lisätty karkeasuunnitelmaan elokuvasta rivi, jota ei olisi elokuvat.txt tiedostossa
    elokuvat = []
    e_nimi = []
    with open ("elokuvat.txt", "r") as elokuvat_tiedosto: # elokuva, genre, kesto
        elokuvat = elokuvat_tiedosto.readlines()
        for rivi in elokuvat:
            elokuvatiedot_osat = rivi.strip().split(", ")
            e_nimi.append(elokuvatiedot_osat[0])
    with open("karkeasuunnitelmaa.txt", "r") as karkeasuunnitelmaa_tiedosto: # aloitusviikko, paatosviikko, elokuva, genre, kesto
        for rivi in karkeasuunnitelmaa_tiedosto:
            karkeasuunnitelmaa_osat = rivi.strip().split(", ")
            if karkeasuunnitelmaa_osat[2] not in e_nimi: # jos elokuvat.txt tiedostossa ei olisi karkeasuunnitelmaa.txt tiedostossa olevaa elokuvaa, niin lisäys automaattisesti (jos muokattu manuaalisesti karkeasuunnitelmaa.txt tiedostoa)
                elokuvat.append(f"{karkeasuunnitelmaa_osat[2]}, {karkeasuunnitelmaa_osat[3]}, {karkeasuunnitelmaa_osat[4]}\n")
    elokuvat.sort(key=lambda elokuvatiedot_osat: elokuvatiedot_osat[0])
    with open("elokuvat.txt", "w") as elokuvat_tiedosto:
        elokuvat_tiedosto.writelines(elokuvat)

def karkeasuunnitelmaa_autom_paivitys(): # (käytetään modulaarisena aliohjelmana)
    # tehdään automaattisesti karkeasuunnitelmaa.txt päivitys, jos lisätty elokuva, jota ei olisi karkeasuunnitelmaa.txt tiedostossa tai poistettu elokuva ohjalmasta
    elokuvat_karkeasuunnitelmaa = []
    k_nimi = []

    with open("karkeasuunnitelmaa.txt", "r") as karkeasuunnitelmaa_tiedosto: # aloitusviikko, paatosviikko, elokuva, genre, kesto
        for rivi in karkeasuunnitelmaa_tiedosto:
            karkeasuunnitelmaa_osat = rivi.strip().split(", ")
            k_nimi.append(karkeasuunnitelmaa_osat[2])
    with open("elokuvat.txt", "r") as elokuvat_tiedosto: # elokuva, genre, kesto
        for rivi in elokuvat_tiedosto:
            elokuvatiedot_osat = rivi.strip().split(", ")
            if elokuvatiedot_osat[0] not in k_nimi: # jos karkeasuunnitelmaa.txt tiedostossa ei olisi elokuvat.txt tiedostossa olevalle elokuvalle riviä, niin elokuva-rivin lisääminen karkeasuunnitelmaan automaattisesti
                elokuvat_karkeasuunnitelmaa.append(f"{".."}, {".."}, {elokuvatiedot_osat[0]}, {elokuvatiedot_osat[1]}, {elokuvatiedot_osat[2]}\n")
            else:
                with open("karkeasuunnitelmaa.txt", "r") as karkeasuunnitelmaa_tiedosto:
                    for rivi in karkeasuunnitelmaa_tiedosto:
                        karkeasuunnitelmaa_osat = rivi.strip().split(", ")
                        if elokuvatiedot_osat[0] == karkeasuunnitelmaa_osat[2]:
                            elokuvat_karkeasuunnitelmaa.append(f"{karkeasuunnitelmaa_osat[0]}, {karkeasuunnitelmaa_osat[1]}, {elokuvatiedot_osat[0]}, {elokuvatiedot_osat[1]}, {elokuvatiedot_osat[2]}\n")
    elokuvat_karkeasuunnitelmaa.sort(key=lambda karkeasuunnitelmaa_osat: karkeasuunnitelmaa_osat[0]) # muutetaan järjestykseen aloitusviikon perusteella
    with open("karkeasuunnitelmaa.txt", "w") as karkeasuunnitelmaa_tiedosto:
        karkeasuunnitelmaa_tiedosto.writelines(elokuvat_karkeasuunnitelmaa) 

def nayta_tallennetut_elokuvat(elokuva: str, genre: str, kesto) -> list:
    elokuvat_txt_tarkistus_paivitys()
    karkeasuunnitelmaa_tarkistus_paivitys()
    elokuvat_autom_lisayspaivitys()
    karkeasuunnitelmaa_autom_paivitys()
    print(" Alla ohjelmassa tallentuneina olevat elokuvatiedot (elokuva, genre, kesto):")
    with open("elokuvat.txt", "r") as elokuvat_tiedosto: # elokuva, genre, kesto
        for rivi in elokuvat_tiedosto:
            elokuvat_osat = rivi.strip().split(", ")
            print(f" {elokuvat_osat[0][0].upper() + elokuvat_osat[0][1:]}, {elokuvat_osat[1]}, {elokuvat_osat[2]}")

def lisaa_elokuva_taimuokkaa(elokuva, genre, kesto):
    elokuvat_txt_tarkistus_paivitys()
    karkeasuunnitelmaa_tarkistus_paivitys()
    elokuvat_autom_lisayspaivitys()
    karkeasuunnitelmaa_autom_paivitys()
    print(" Alla ohjelmassa tällä hetkellä tallentuneina olevat elokuvatiedot (elokuva, genre, kesto):")
    with open("elokuvat.txt", "r") as elokuvat_tiedosto: # elokuva, genre, kesto
        for rivi in elokuvat_tiedosto:
            elokuvat_osat = rivi.strip().split(", ")
            print(f" {elokuvat_osat[0]}, {elokuvat_osat[1]}, {elokuvat_osat[2]}")
    print("")
    while True:
        elokuva = input("Anna lisättävän/muokattavan elokuvan nimi (tyhjä=poistu): ")
        if elokuva == "":
            break
        else:
            elokuva = elokuva.strip().lower() # tallennetaan ohjelmaan kaikki samanmuotoisesti pienillä kirjaimilla ja poistetaan mahd. välilyönnit
            break
    while True:
        if elokuva == "":
            break
        genre = input("Anna elokuvan genre: ")
        if genre == "":
            print("Tämä ei voisi olla tyhjä, lisää genre")
        else:
            genre = genre.strip().lower() # tallennetaan ohjelmaan kaikki samanmuotoisesti pienillä kirjaimilla ja poistetaan mahd. välilyönnit
            break
    from datetime import datetime
    while True:
        try:
            if elokuva == "":
                break
            else:
                kesto = input("Anna elokuvan kesto (tt:mm): ")
                kesto = kesto.strip()
                kesto = datetime.strptime(kesto, '%H:%M')
                kesto = kesto.strftime('%H:%M')
                break
        except ValueError:
            virheilmoitus("Kesto pitäisi antaa tt:mm muodossa, esim. 1:30")
            continue
    with open("elokuvat.txt", "r") as elokuvat_tiedosto: # elokuva, genre, kesto
        a = elokuvat_tiedosto.readlines()
    elokuvat = []
    while True:
        if elokuva == "":
            break
        else:
            for rivi in a:
                elokuvatiedot_osat = rivi.strip().split(", ")
                if elokuvatiedot_osat == "":
                    elokuvat.append(f"{elokuva}, {genre}, {kesto}\n")
                elif elokuvatiedot_osat[0] != elokuva:
                    elokuvat.append(", ".join(elokuvatiedot_osat) + "\n")
                elif elokuvatiedot_osat[0] == elokuva:
                    elokuvatiedot_osat[1] = genre
                    elokuvatiedot_osat[2] = kesto
            if elokuva not in elokuvat:
                elokuvat.append(f"{elokuva}, {genre}, {kesto}\n")
            elokuvat.sort(key=lambda elokuvatiedot_osat: elokuvatiedot_osat[0]) # muutetaan aakkosjärjestykseen elokuvan nimen perusteella
            with open("elokuvat.txt", "w") as elokuvat_tiedosto:
                elokuvat_tiedosto.writelines(elokuvat)
            break
    karkeasuunnitelmaa_autom_paivitys()
    with open("elokuvat.txt", "r") as elokuvat_tiedosto: # elokuva, genre, kesto
        b = elokuvat_tiedosto.readlines()
        if a == b:
            print(" Tallennettuihin elokuviin ei tässä tehty muutoksia")
        else:
            print(f" Elokuvatiedot '{elokuva}' tallennettiin/muutettiin ohjelmaan (elokuvat.txt, karkeasuunnitelmaa.txt)")             

def poista_elokuva(elokuva: str, genre: str, kesto):
    elokuvat_txt_tarkistus_paivitys()
    karkeasuunnitelmaa_tarkistus_paivitys()
    elokuvat_autom_lisayspaivitys()
    karkeasuunnitelmaa_autom_paivitys()
    print(" Alla ohjelmassa tällä hetkellä tallentuneina olevat elokuvatiedot (elokuva, genre, kesto):")
    with open("elokuvat.txt", "r") as elokuvat_tiedosto: # elokuva, genre, kesto
        for rivi in elokuvat_tiedosto:
            elokuvat_osat = rivi.strip().split(", ")
            print(f" {elokuvat_osat[0]}, {elokuvat_osat[1]}, {elokuvat_osat[2]}")
    print("")
    while True:
        elokuva_poista = input("Anna poistettavan elokuvan nimi (tyhjä=poistu): ") 
        if elokuva_poista == "":
            print(" Tallennettuihin elokuviin ei tässä tehty muutoksia")
            break
        elokuva_poista = elokuva_poista.strip().lower() # mahd. muutetaan pieniksi kirjaimiksi, miten myös tallennetuissa elokuvissa sekä poistetaan mahd. välilyönnit
        elokuvat = []
        with open("elokuvat.txt", "r") as elokuvat_tiedosto: # elokuva, genre, kesto
            a = elokuvat_tiedosto.readlines()
        for rivi in a:
            elokuvatiedot_osat = rivi.strip().split(", ")
            if elokuvatiedot_osat[0] != elokuva_poista:
                elokuvat.append(", ".join(elokuvatiedot_osat) + "\n")
            elif elokuvatiedot_osat[0] == elokuva_poista:
                elokuvat.append("")
        with open("elokuvat.txt", "w") as elokuvat_tiedosto:
            elokuvat_tiedosto.writelines(elokuvat)
        karkeasuunnitelmaa_autom_paivitys()
        with open("elokuvat.txt", "r") as elokuvat_tiedosto:
            b = elokuvat_tiedosto.readlines()
            if len(a) > len(b):
                print(f" Elokuva '{elokuva_poista}' ja liittyvät tiedot poistettiin ohjelmasta (elokuvat.txt, karkeasuunnitelmaa.txt)")
                break
            elif len(a) == len(b):
                print(f" Elokuvan nimeä '{elokuva_poista}' ei löydy ohjelmasta, ks. toiminto '1. Näytä ohjelmaan tallennetut elokuvat', onkohan samoin kirjoitettu?")
                print(" Elokuvan voi myös poistaa tallennetuista manuaalisesti poistamalla kyseisen rivin elokuvat.txt tiedostosta")
                print(" Tallennettuihin elokuviin ei tässä tehty muutoksia")
        break
    
def nayta_karkeasuunnitelmaa(aloitusviikko: int, paatosviikko: int, elokuva: str, genre: str, kesto: str) -> list:
    # tarkistetaan elokuvat.txt ja karkeasuunnitelmaa.txt tiedostot ja mahd. päivitetään (mm. jos mahd. manuaalisesti lisättyihin olisi jäänyt ylimääräisiä rivejä, välilyöntejä tai samasta elokuvasta eri rivejä)
    elokuvat_txt_tarkistus_paivitys()
    karkeasuunnitelmaa_tarkistus_paivitys()

    # tehdään automaattisesti elokuvat.txt ja karkeasuunnitelmaa.txt päivitykset, jos manuaalisesti oltaisiin lisätty karkeasuunnitelmaan elokuvasta rivi, jota ei olisi elokuvat.txt tiedostossa
    # tai jos lisätty ohjelmaan tai elokuvat.txt tiedostoon elokuva, jota ei olisi karkeasuunnitelmaa.txt tiedostossa tai jos elokuva olisi poistettu
    elokuvat_autom_lisayspaivitys()
    karkeasuunnitelmaa_autom_paivitys()
    print(" Alla ohjelmaan tallennetut elokuvat ja tämän hetkistä karkeasuunnitelmaa (aloitusviikko, päätösviikko, elokuva, genre, kesto)")
    with open("karkeasuunnitelmaa.txt", "r") as karkeasuunnitelmaa_tiedosto: # aloitusviikko, paatosviikko, elokuva, genre, kesto
        for rivi in karkeasuunnitelmaa_tiedosto:
            rivi_osat = rivi.strip().split(", ")
            print(f" {rivi_osat[0]}, {rivi_osat[1]},  {rivi_osat[2]}, {rivi_osat[3]}, {rivi_osat[4]}")
    print("")

def tee_karkeasuunnitelmaa(aloitusviikko: int, paatosviikko: int, elokuva: str, genre: str, kesto) -> list:
    # tarkistetaan ensin elokuvat.txt ja karkeasuunnitelmaa.txt tiedostot, tehdään mahd. automaattinen päivitys ja näytetään tämän hetkiset alustavat aloitusviikko-päätösviikko tiedot 
    # (mm. jos manuaalisesti lisättyihin olisi jäänyt ylimääräisiä rivejä, välilyöntejä, samasta elokuvasta eri rivejä, tiedostoissa eri elokuvia tai elokuva/elokuvia olisi poistettu)
    elokuvat_txt_tarkistus_paivitys()
    karkeasuunnitelmaa_tarkistus_paivitys()
    elokuvat_autom_lisayspaivitys()
    karkeasuunnitelmaa_autom_paivitys()
    while True:
        print(" Alla ohjelmaan tallennetut elokuvat ja tämän hetkistä karkeasuunnitelmaa (aloitusviikko, päätösviikko, elokuva, genre, kesto)")
        elokuvat_karkeasuunnitelmaa = []
        k_nimi = []
        with open("karkeasuunnitelmaa.txt", "r") as karkeasuunnitelmaa_tiedosto: # aloitusviikko, paatosviikko, elokuva, genre, kesto
            for rivi in karkeasuunnitelmaa_tiedosto:
                karkeasuunnitelmaa_osat = rivi.strip().split(", ")
                elokuvat_karkeasuunnitelmaa.append(", ".join(karkeasuunnitelmaa_osat) + "\n")
                k_nimi.append(karkeasuunnitelmaa_osat[2])
                print(f" {karkeasuunnitelmaa_osat[0]}, {karkeasuunnitelmaa_osat[1]},  {karkeasuunnitelmaa_osat[2]}, {karkeasuunnitelmaa_osat[3]}, {karkeasuunnitelmaa_osat[4]}") # tässä yksi välilyönti lisätty
        with open("karkeasuunnitelmaa.txt", "w") as karkeasuunnitelmaa_tiedosto: 
            karkeasuunnitelmaa_tiedosto.writelines(elokuvat_karkeasuunnitelmaa)
        print("")

        while True:
            elokuva = input("Anna elokuvan nimi, mihin lisätään/muokataan näytösten aloitusviikko-päätösviikko aikoja (alustavia) (tyhjä=poistu): ")
            if elokuva == "":
                break
            elokuva = elokuva.strip().lower() # tallennetaan ohjelmaan kaikki samanmuotoisesti pienillä kirjaimilla ja poistetaan mahd. välilyönnit
            if elokuva not in k_nimi:
                print(" Elokuvaa ei löytynyt ohjelmasta (elokuvat.txt, karkeasuunnitelmaa.txt), onkohan samoin kirjoitettu kuin yllä listassa? (tai pitäisi ensin mahd. lisätä)")
            break
        from datetime import datetime
        while True:
            if elokuva == "" or elokuva not in k_nimi:
                break
            try:
                aloitusviikko_str = input("Anna viikon numero, milloin elokuvaa alustavasti alettaisiin näyttämään viikko-ohjelmassa? (tyhjällä ei merkintää): ")
                if aloitusviikko_str == "":
                    break
                else:
                    aloitusviikko = int(aloitusviikko_str)
                    if aloitusviikko <= 0 or aloitusviikko > 53: # jos karkausvuosi, niin 53 viikkoa
                        print(" Viikkonumeron tulisi olla välillä 1-53")
                    else:
                        break
            except ValueError:
                virheilmoitus("Aloitusviikko tulisi antaa viikkonumerona")
                continue
        while True:
            if elokuva == "" or elokuva not in k_nimi:
                break
            try:
                paatosviikko_str = input("Anna alustava päätösviikon numero, jonka jälkeen elokuvaa ei enää näytettäisi viikko-ohjelmassa? (tyhjällä ei merkintää): ")
                if paatosviikko_str == "":
                    break
                else:
                    paatosviikko = int(paatosviikko_str)
                    if paatosviikko <= 0 or paatosviikko > 53: # jos karkausvuosi, niin 53 viikkoa
                        print(" Viikkonumeron tulisi olla välillä 1-53")
                    elif paatosviikko < aloitusviikko:
                        print(" Päätösviikko ei voisi olla ennen aloitusviikkoa")
                    else:
                        break
            except ValueError:
                virheilmoitus("Päätösviikko (alustava) tulisi antaa viikkonumerona")
                continue
        elokuvat_karkeasuunnitelmaa = []
        while True:
            if elokuva == "" or elokuva not in k_nimi:
                break
            with open("karkeasuunnitelmaa.txt", "r") as karkeasuunnitelmaa_tiedosto: # aloitusviikko, paatosviikko, elokuva, genre, kesto
                a = karkeasuunnitelmaa_tiedosto.readlines
                for rivi in karkeasuunnitelmaa_tiedosto:
                    karkeasuunnitelmaa_osat = rivi.strip().split(", ")
                    if karkeasuunnitelmaa_osat[2] == elokuva and aloitusviikko_str != "" and paatosviikko_str != "":
                        elokuvat_karkeasuunnitelmaa.append(f"{aloitusviikko}, {paatosviikko}, {karkeasuunnitelmaa_osat[2]}, {karkeasuunnitelmaa_osat[3]}, {karkeasuunnitelmaa_osat[4]}\n")
                    elif karkeasuunnitelmaa_osat[2] == elokuva and aloitusviikko_str != "" and paatosviikko_str == "":
                        elokuvat_karkeasuunnitelmaa.append(f"{aloitusviikko}, {".."}, {karkeasuunnitelmaa_osat[2]}, {karkeasuunnitelmaa_osat[3]}, {karkeasuunnitelmaa_osat[4]}\n")
                    elif karkeasuunnitelmaa_osat[2] == elokuva and aloitusviikko_str == "" and paatosviikko_str != "":
                        elokuvat_karkeasuunnitelmaa.append(f"{".."}, {paatosviikko}, {karkeasuunnitelmaa_osat[2]}, {karkeasuunnitelmaa_osat[3]}, {karkeasuunnitelmaa_osat[4]}\n")
                    elif karkeasuunnitelmaa_osat[2] == elokuva and aloitusviikko_str == "" and paatosviikko_str == "":
                        elokuvat_karkeasuunnitelmaa.append(f"{".."}, {".."}, {karkeasuunnitelmaa_osat[2]}, {karkeasuunnitelmaa_osat[3]}, {karkeasuunnitelmaa_osat[4]}\n")
                    elif karkeasuunnitelmaa_osat[2] != elokuva:
                        elokuvat_karkeasuunnitelmaa.append(f"{karkeasuunnitelmaa_osat[0]}, {karkeasuunnitelmaa_osat[1]}, {karkeasuunnitelmaa_osat[2]}, {karkeasuunnitelmaa_osat[3]}, {karkeasuunnitelmaa_osat[4]}\n")
                with open("karkeasuunnitelmaa.txt", "w") as karkeasuunnitelmaa_tiedosto:
                    karkeasuunnitelmaa_tiedosto.writelines(elokuvat_karkeasuunnitelmaa)
                with open("karkeasuunnitelmaa.txt", "r") as karkeasuunnitelmaa_tiedosto:
                    b = karkeasuunnitelmaa_tiedosto.readlines()
                if a != b:
                    print("")
                    print(" Karkeasuunnitelmaa päivitettiin")
                    break
                elif a == b:
                    print(" Ei tehty tässä muutoksia")
        if elokuva == "" or elokuva not in k_nimi:
            break

def nayta_viikko_ohjelma(viikko_nro: int, pvm, viikonpaiva, tunnistenro: int, elokuva: str, salinro: int, aloitusaika, paattymisaika) -> list:
    from datetime import datetime
    viikko_nro_ohjelma = []
    while True:
        try:
            viikko_nro_str = input("Minkä viikon viikko-ohjelma näytetään? (tyhjä=poistu) Viikkonumero: ")
            if viikko_nro_str == "":
                break
            else:
                viikko_nro = int(viikko_nro_str)
                viikko_nro_tanaan = int(datetime.now().strftime('%W'))
                while True:
                    try:
                        viikko_vuosi_str = input("ja vuosi (tyhjä=poistu): ")
                        if viikko_vuosi_str == "":
                            break
                        else:
                            viikko_vuosi = int(viikko_vuosi_str)
                            break
                    except ValueError:
                        virheilmoitus("Vuosi tulisi antaa numerona, esim. 2024")
                        print("")
                        continue
                if viikko_vuosi_str == "":
                    break              
                with open("viikko_ohjelmat.txt", "r") as viikko_ohjelmat_tiedosto: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                    for rivi in viikko_ohjelmat_tiedosto:
                        vko_ohj_osat = rivi.strip().split(", ")
                        if vko_ohj_osat[0] == str(viikko_nro) and vko_ohj_osat[1].split(".")[2] == str(viikko_vuosi):
                            viikko_nro_ohjelma.append(", ".join(vko_ohj_osat) + "\n")
                if viikko_nro <= 0 or viikko_nro > 53: # jos karkausvuosi, niin 53 viikkoa
                    print(" Viikkonumeron tulisi olla välillä 1-53")
                    print("")
                elif viikko_nro_ohjelma == []: # eli tyhjä
                    print(f" Valitettavasti viikko-ohjelmaa viikolta {viikko_nro}/{viikko_vuosi} ohjelmassa ei ole saatavilla, tarkistatko vielä viikkonumeron")
                    print(f" (tämän viikon {viikko_nro_tanaan} sekä mahd. seuraavan viikon viikko-ohjelma kuitenkin pitäisi löytyä)")
                    print("")
                elif viikko_nro_ohjelma != []: # eli jos ei tyhjä 
                    print("")
                    print(f" Alla listana viikon {viikko_nro}/{viikko_vuosi} viikko-ohjelma ja näytöstiedot (viikonpäivä, päivämäärä, näytöksen tunnistenumero, elokuva, salinumero, aloitusaika - päättymisaika):")
                    for rivi in viikko_nro_ohjelma:
                        vko_nro_ohj_osat = rivi.strip().split(", ")
                        print(f" {vko_nro_ohj_osat[2]}, {vko_nro_ohj_osat[1]}, {vko_nro_ohj_osat[3]}, {vko_nro_ohj_osat[4][0].upper() + vko_nro_ohj_osat[4][1:]}, {vko_nro_ohj_osat[5]}, {vko_nro_ohj_osat[6]} - {vko_nro_ohj_osat[7]}")
                    print("")
                    break
        except ValueError:
            virheilmoitus("Viikkonumero pitäisi antaa numerona")
            print("")
            continue

def tee_viikko_ohjelmaa(viikko_nro: int, pvm, viikonpaiva, tunnistenro: int, elokuva: str, salinro: int, aloitusaika, paattymisaika) -> list:
    # tarkistetaan ensin elokuvat.txt ja karkeasuunnitelmaa.txt tiedostot, tehdään mahd. automaattinen päivitys ja näytetään tämän hetkiset alustavat aloitusviikko-päätösviikko tiedot 
    # (mm. jos manuaalisesti lisättyihin olisi jäänyt ylimääräisiä rivejä, välilyöntejä, samasta elokuvasta eri rivejä, tiedostoissa eri elokuvia tai elokuva/elokuvia olisi poistettu)
    # näytetään ohjelmaan tallennetut elokuvat ja tämän hetkistä karkeasuunnitelmaa
    nayta_karkeasuunnitelmaa('aloitusviikko', 'paatosviikko', 'elokuva', 'genre', 'kesto')
    from datetime import datetime
    
    # ohjelmassa olevien elokuvien nimet listaan:
    k_nimi = []
    with open("karkeasuunnitelmaa.txt", "r") as karkeasuunnitelmaa_tiedosto: # aloitusviikko, paatosviikko, elokuva, genre, kesto
        for rivi in karkeasuunnitelmaa_tiedosto:
            karkeasuunnitelmaa_osat = rivi.strip().split(", ")
            k_nimi.append(karkeasuunnitelmaa_osat[2])
    
    # lisätään "kokonaissilmukkana" viikko-ohjelmaan(/-ohjelmiin) näytöksiä tai muokataan:
    # valitaan viikko (viikkonumero) (ja myös vuosi), mihin lisätään/muokataan näytöksiä
    while True:
        viikko_nro_ohjelma = []
        viikko_ohjelmat = []
        while True:
            try:
                viikko_nro_str = input("Mille viikolle tehdään viikko_ohjelmaa? Anna viikon numero (tyhjä=poistu): ")
                if viikko_nro_str == "":
                    break
                else:
                    viikko_nro = int(viikko_nro_str)
                    viikko_nro_tanaan = int(datetime.now().strftime('%W'))
                    while True:
                        try:
                            viikko_vuosi_str = input("Mille vuodelle? (tyhjä=poistu): ")
                            if viikko_vuosi_str == "":
                                break
                            else:
                                viikko_vuosi = int(viikko_vuosi_str)
                                viikko_vuosi_tanaan = int(datetime.now().strftime('%Y'))
                                if viikko_vuosi < viikko_vuosi_tanaan:
                                    print(" Tarkistatko vielä vuoden (aiempien vuosien viikko-ohjelmia ei voisi tässä muokata)")
                                    print("")
                                else:
                                    break
                        except ValueError:
                            virheilmoitus("Vuosi tulisi antaa numerona, esim. 2024")
                            print("")
                            continue
                    if viikko_vuosi_str == "":
                        break
                    elif viikko_nro <= viikko_nro_tanaan and viikko_vuosi == viikko_vuosi_tanaan:  # nyt laitettu näin, tosin saattaisi myös tulla poikkeuksia/tarpeita tehdä muutoksia viikko-ohjelmaan viikko-ohjelmaviikon aikana
                        print(" Tämän hetkistä viikko-ohjelmaa tai aiempia ei voisi tässä muokata")
                        print("")
                    else:
                        break
            except ValueError:
                virheilmoitus("Viikkonumero tulisi antaa numerona")
                print("")
                continue

        # näytetään kyseisen viikkonumeron tämän hetkinen päivittynyt viikko-ohjelmatilanne/viikko_ohjelmassa olevat näytökset
        while True:
            if viikko_nro_str == "" or viikko_vuosi_str == "": # jos heti aluksi oltaisiin annettu tyhjä =poistu
                break
            with open("viikko_ohjelmat.txt", "r") as viikko_ohjelmat_tiedosto: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                for rivi in viikko_ohjelmat_tiedosto:
                    vko_ohj_osat = rivi.strip().split(", ")
                    if vko_ohj_osat[0] == str(viikko_nro) and vko_ohj_osat[1].split(".")[2] == str(viikko_vuosi):
                        viikko_nro_ohjelma.append(", ".join(vko_ohj_osat) + "\n")
                if viikko_nro_ohjelma == []: # eli tyhjä
                    print(f" Tällä hetkellä viikolle {viikko_nro}/{viikko_vuosi} ei ole vielä tallennettuna näytöksiä, nyt tallennettaisiin ensimmäinen näytös")
                    print("")
                    break
                else:
                    print("")
                    print(f" Viikon {viikko_nro}/{viikko_vuosi} tämän hetkinen viikko-ohjelma (päivämäärä, viikonpäivä, näytöksen tunnistenumero, elokuva, salinumero, aloitusaika - päättymisaika):")
                    for rivi in viikko_nro_ohjelma:
                        vko_nro_ohj_osat = rivi.strip().split(", ")
                        print(f" {vko_nro_ohj_osat[1]}, {vko_nro_ohj_osat[2]}, {vko_nro_ohj_osat[3]}, {vko_nro_ohj_osat[4][0].upper() + vko_nro_ohj_osat[4][1:]}, {vko_nro_ohj_osat[5]}, {vko_nro_ohj_osat[6]} - {vko_nro_ohj_osat[7]}")
                    print("")
                    break
        
        # valitaan lisättävän näytöksen päivämäärä (päivämäärällä automaattisesti myös viikonpäivä)
        while True:
            if viikko_nro_str == "" or viikko_vuosi_str == "":
                break
            try:
                naytos_pvm_annettu = input("Mille päivämäärälle lisätään näytös? (muodossa pp.kk.vvvv) (tyhjä=poistu): ")
                if naytos_pvm_annettu == "":
                    break
                else:
                    naytos_pvm_annettu = naytos_pvm_annettu.strip() # poistetaan mahd. välilyönnit
                    naytos_pvm_strptime = datetime.strptime(naytos_pvm_annettu, '%d.%m.%Y')
                    naytos_pvm = naytos_pvm_strptime.strftime('%d.%m.%Y')
                    naytoksen_viikonpaiva = naytos_pvm_strptime.strftime('%A')
                    naytos_viikko_nro = int(naytos_pvm_strptime.strftime('%W'))
                    naytos_vuosi = int(naytos_pvm_strptime.strftime('%Y'))
                    vuosi_tanaan = int(datetime.now().strftime('%Y'))
                    if naytos_viikko_nro <= viikko_nro_tanaan and naytos_vuosi == vuosi_tanaan: # (viikko_nro on suurempi, mitä viikko_nro tänään) (tällä hetkellä laitettu näin, tosin saattaisi myös tulla poikkeuksia/tarpeita tehdä muutoksia viikko-ohjelmaan viikko-ohjelmaviikon aikana)
                        print(" Tämän hetkistä viikko-ohjelmaa tai aiempia ei voisi tässä muokata")
                        print("")
                    elif naytos_vuosi < vuosi_tanaan:
                        print(" Tarkistatko vuoden, aiempien vuosien viikko-ohjelmia ei voisi tässä muokata")
                        print("")
                    elif naytos_viikko_nro != viikko_nro:
                        print(f" Aiemmin yllä annettiin viikoksi {viikko_nro} (/{viikko_vuosi}) ja mistä näytettiin tämän hetkiset viikko-ohjelmatiedot, mutta päivämäärä {naytos_pvm} olisi viikolla {naytos_viikko_nro}.")
                        print(f" Tässä tarvitsisi varmuudeksi olla sama viikko kuin yllä. Onkohan päivämäärä oikein? (tai sitten tarvitsisi poistua ja aloittaa lisäämään viikkonumerolla {naytos_viikko_nro})")
                        print("")
                    else:
                        print("")
                        break
            except ValueError:
                virheilmoitus("Päivämäärä tulisi antaa muodossa pp.kk.vvvv, esim. 20.2.2024")
                print("")
                continue

        # valitaan elokuva
        kesto = ""
        genre = ""
        while True:
            if viikko_nro_str == "" or viikko_vuosi_str == "" or naytos_pvm_annettu == "":
                break
            else:
                elokuva_naytoksessa = input("Mikä on näytöksen elokuvan nimi, mikä lisätään viikko-ohjelmaan? (tyhjä=poistu): ")
                elokuva_naytoksessa = elokuva_naytoksessa.strip().lower() # tallennetaan ohjelmaan kaikki samanmuotoisesti pienillä kirjaimilla ja poistetaan mahd. välilyönnit
                if elokuva_naytoksessa == "":
                    break
                elif elokuva_naytoksessa not in k_nimi: # ylempänä ennen "kokonaissilmukkaa" poimittu karkeasuunnitelmaa.txt tiedostosta
                    print(f" Elokuvaa '{elokuva_naytoksessa}' ei löydy ohjelmasta, onkohan samoin kirjoitettu kuin yllä listassa? (tai elokuvatiedot pitäisi ensin lisätä ohjelmaan)")
                    print("")
                else:
                    with open("karkeasuunnitelmaa.txt", "r") as karkeasuunnitelmaa_tiedosto: # aloitusviikko, paatosviikko, elokuva, genre, kesto
                        elokuvat_karkeasuunnitelmaa = karkeasuunnitelmaa_tiedosto.readlines()
                        for i in range(len(elokuvat_karkeasuunnitelmaa)):
                            karkeasuunnitelmaa_osat = elokuvat_karkeasuunnitelmaa[i].strip().split(", ")
                            if karkeasuunnitelmaa_osat[2] == elokuva_naytoksessa:
                                kesto = karkeasuunnitelmaa_osat[4]
                                print(f" Elokuvan '{karkeasuunnitelmaa_osat[2]}' tietoja: genrenä on '{karkeasuunnitelmaa_osat[3]}' ja kesto on '{karkeasuunnitelmaa_osat[4]}'")
                        print("")
                        break                  
        
        # valitaan näytöksen aloitusaika (päättymisaika muodostuu automaattisesti aloitusaika + elokuvan kesto)
        while True:
            if viikko_nro_str == "" or viikko_vuosi_str == "" or naytos_pvm_annettu == "" or elokuva_naytoksessa == "":
                break
            try:
                naytoksen_aloitusaika_annettu = input("Mihin kellonaikaan näytös aloitetaan? (tt:mm) (tyhjä=poistu): ")
                if naytoksen_aloitusaika_annettu == "":
                    break
                else:
                    naytoksen_aloitusaika_annettu = naytoksen_aloitusaika_annettu.strip()
                    naytoksen_aloitusaika_strptime = datetime.strptime(naytoksen_aloitusaika_annettu, '%H:%M')
                    naytoksen_aloitusaika = naytoksen_aloitusaika_strptime.strftime('%H:%M')
                    elokuvan_kesto = kesto # karkeasuunnitelmaa.txt tiedostosta, vähän ylemmpänä
                    naytoksen_paattymisaika_tunnit_int = int(naytoksen_aloitusaika.split(":")[0]) + int(elokuvan_kesto.split(":")[0])
                    naytoksen_paattymisaika_min_yht_int = int(naytoksen_aloitusaika.split(":")[1]) + int(elokuvan_kesto.split(":")[1]) # tehty näin huomioimaan myös, jos yhteenlasketuista minuuteista tulisi yli 60
                    if naytoksen_paattymisaika_min_yht_int >= 60:
                        naytoksen_paattymisaika_min_str = str(naytoksen_paattymisaika_min_yht_int - 60)
                        naytoksen_paattymisaika_tunti_str = str(naytoksen_paattymisaika_tunnit_int + 1)
                    elif naytoksen_paattymisaika_min_yht_int < 60:
                        naytoksen_paattymisaika_min_str = str(naytoksen_paattymisaika_min_yht_int)
                        naytoksen_paattymisaika_tunti_str = str(naytoksen_paattymisaika_tunnit_int)
                    naytoksen_paattymisaika_min_strptime = datetime.strptime(naytoksen_paattymisaika_min_str, '%M') # muutetaan kahden numeron muotoon, esim 01 (esim. 16:30 + 33 että tulisi 17:03, eikä 17:3)
                    naytoksen_paattymisaika_min = naytoksen_paattymisaika_min_strptime.strftime('%M') # muutetaan kahden numeron muotoon, esim 01 (esim. 16:30 + 33 että tulisi 17:03, eikä 17:3)
                    naytoksen_paattymisaika_tunti_strptime = datetime.strptime(naytoksen_paattymisaika_tunti_str, '%M')
                    naytoksen_paattymisaika_tunti = naytoksen_paattymisaika_tunti_strptime.strftime('%M')
                    naytoksen_paattymisaika_osat = [naytoksen_paattymisaika_tunti, naytoksen_paattymisaika_min]
                    naytoksen_paattymisaika = ":".join(naytoksen_paattymisaika_osat)
                    naytoksen_paattymisaika_strptime = datetime.strptime(naytoksen_paattymisaika, '%H:%M')
                    tarkistus = input(f" Ohjelman mukaan näytös loppuu kello {naytoksen_paattymisaika}, menikö oikein? (k/e): ")
                    if tarkistus == "k":
                        print(f" Elokuvan {elokuva_naytoksessa[0].upper() + elokuva_naytoksessa[1:]} näytösaikana olisi {naytoksen_aloitusaika} - {naytoksen_paattymisaika} ({naytoksen_viikonpaiva} {naytos_pvm})") # tässä nyt elokuvan kestoaika, missä myös elokuvan lopputekstit, muttei esim. näytöksen alkumainoksia/muita
                        print("")
                        break
                    elif tarkistus == "e":
                        print(" Onkohan elokuvan kesto väärin ohjelmassa? Tarkistatko vielä, päättymisajan tulisi mennä oikein (elokuvat.txt, karkeasuunnitelmaa.txt)")
                        print("")
                    else:
                        print(" Pitäisi antaa muodossa kirjain k tai e")
                        print("")
            except ValueError:
                virheilmoitus("Kellonaika tulisi antaa muodossa tt:mm, esim. 16:30")
                print("")
                continue

        # valitaan salinumero
        # ja tarkistetaan, ettei voisi vahingossa olla päällekkäisyyttä salin tallennettuihin näytöksiin ja jos ei päällekkäisyyttä niin muodostetaan näytökselle tunnistenumero ja tallennetaan näytös ohjelmaan (viikko-ohjelmat.txt)
        while True:
            s_nro = []
            viikko_ohjelmat = []
            if viikko_nro_str == "" or viikko_vuosi_str == "" or naytos_pvm_annettu == "" or elokuva_naytoksessa == "" or naytoksen_aloitusaika_annettu == "":
                break
            try: 
                naytoksen_salinro_str = input("Missä salissa näytös pidetään? (nro) (tyhjä=poistu): ")
                if naytoksen_salinro_str == "":
                    break
                else:
                    naytoksen_salinro = int(naytoksen_salinro_str)
                    with open("viikko_ohjelmat.txt", "r") as viikko_ohjelmat_tiedosto:
                        for rivi in viikko_ohjelmat_tiedosto:
                            viikko_ohjelmat_osat = rivi.strip().split(", ")
                            viikko_ohjelmat.append(", ".join(viikko_ohjelmat_osat) + "\n")
                    with open("salitiedot.txt", "r") as salitiedot_tiedosto: # salinro, istumapaikkalkm, rivimaara, istumapaikkoja_per_rivi, muita_salitietoja
                        salitiedot = salitiedot_tiedosto.readlines()
                        for rivi in salitiedot:
                            salitiedot_osat = rivi.strip().split(", ")
                            s_nro.append(salitiedot_osat[0])
                    if str(naytoksen_salinro) not in s_nro: 
                        print(f" Salia '{naytoksen_salinro}' ei löydy ohjelmasta, meniköhän numero oikein? (tai salitiedot pitäisi ensin lisätä ohjelmaan)")
                        print("")
                    elif str(naytoksen_salinro) in s_nro and viikko_nro_ohjelma == []: # viikko_ohjelman osalta eli jos tyhjä (pitäisi ensin myös tehdä tarkistus, ettei tyhjiä rivejä ja näin tulisi "\n")
                        tunnistenro_naytos = len(viikko_ohjelmat) + 1
                        viikko_ohjelmat.append(f"{viikko_nro}, {naytos_pvm}, {naytoksen_viikonpaiva}, {tunnistenro_naytos}, {elokuva_naytoksessa}, {naytoksen_salinro}, {naytoksen_aloitusaika}, {naytoksen_paattymisaika}\n")
                        with open("viikko_ohjelmat.txt", "w") as viikko_ohjelmat_tiedosto:
                            viikko_ohjelmat_tiedosto.writelines(viikko_ohjelmat)
                            print(f" Näytös lisättiin viikon {viikko_nro} viikko_ohjelmaan (viikko_ohjelmat.txt), alla tiedot:")
                            print(f" Näytöksen päivämäärä: {naytos_pvm}")
                            print(f" Viikonpäivä: {naytoksen_viikonpaiva}")
                            print(f" Näytöksen tunnistenumero: {tunnistenro_naytos}")
                            print(f" Elokuvan nimi: {elokuva_naytoksessa[0].upper() + elokuva_naytoksessa[1:]}")
                            print(f" Salinumero: {naytoksen_salinro}")
                            print(f" Elokuvan näytösaika {naytoksen_aloitusaika} - {naytoksen_paattymisaika}")
                            print("")
                            print("")
                            break
                    elif str(naytoksen_salinro) in s_nro and viikko_nro_ohjelma != []:
                        saman_aikainen = [] 
                        for i in range(len(salitiedot)):
                            salitiedot_osat = salitiedot[i].strip().split(", ")
                            if salitiedot_osat[0] == str(naytoksen_salinro):
                                print(f" Salin tietoja: istumapaikkoja yhteensä {salitiedot_osat[1]}, rivejä {salitiedot_osat[2]}, istumapaikkoja rivillä {salitiedot_osat[3]}, mahd. muita salitietoja: {salitiedot_osat[4]}")
                                print("")
                        for rivi in viikko_nro_ohjelma: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                            vko_ohj_osat = rivi.strip().split(", ")
                            # jos aiemmin tallennettu näytös alkaisi nyt lisättävän näytöksen aikana samassa salissa
                            if vko_ohj_osat[1] == naytos_pvm and vko_ohj_osat[5] == str(naytoksen_salinro) and datetime.strptime(vko_ohj_osat[6], '%H:%M') >= naytoksen_aloitusaika_strptime and datetime.strptime(vko_ohj_osat[6], '%H:%M') <= naytoksen_paattymisaika_strptime:
                                saman_aikainen.append(vko_ohj_osat[3])
                            # jos aiemmin tallennettu näytös päättyisi nyt lisättävän näytöksen aikana samassa salissa
                            elif vko_ohj_osat[1] == naytos_pvm and vko_ohj_osat[5] == str(naytoksen_salinro) and datetime.strptime(vko_ohj_osat[7], '%H:%M') >= naytoksen_aloitusaika_strptime and datetime.strptime(vko_ohj_osat[7], '%H:%M') <= naytoksen_paattymisaika_strptime: 
                                saman_aikainen.append(vko_ohj_osat[3])
                            # jos aiemmin tallennettu näytös alkaisi samassa salissa nyt lisättävän näytöksen aloitusaikaa aiemmin ja myös päättyisi nyt lisättävän näytöksen päättymisajan jälkeen
                            elif vko_ohj_osat[1] == naytos_pvm and vko_ohj_osat[5] == str(naytoksen_salinro) and datetime.strptime(vko_ohj_osat[6], '%H:%M') < naytoksen_aloitusaika_strptime and datetime.strptime(vko_ohj_osat[7], '%H:%M') > naytoksen_paattymisaika_strptime:
                                saman_aikainen.append(vko_ohj_osat[3])
                        if saman_aikainen != []:
                            print(f" Näytöksen lisäys ei onnistunut, saliin {naytoksen_salinro} tulisi ohjelman mukaan päällekkäinen näytös (jompaakumpaa näytöstä pitäisi siirtää, ks. tallennettu tämän hetkinen viikko-ohjelma)")
                            print("")
                        elif saman_aikainen == []:
                            for rivi in viikko_ohjelmat:
                                vko_ohj_osat = rivi.strip().split(", ")
                                if int(vko_ohj_osat[3]) > len(viikko_ohjelmat):
                                    tunnistenro_naytos = int(vko_ohj_osat[3]) + 1
                                else:
                                    tunnistenro_naytos = len(viikko_ohjelmat) + 1
                            viikko_ohjelmat.append(f"{viikko_nro}, {naytos_pvm}, {naytoksen_viikonpaiva}, {tunnistenro_naytos}, {elokuva_naytoksessa}, {naytoksen_salinro}, {naytoksen_aloitusaika}, {naytoksen_paattymisaika}\n")
                            with open("viikko_ohjelmat.txt", "w") as viikko_ohjelmat_tiedosto:
                                viikko_ohjelmat_tiedosto.writelines(viikko_ohjelmat)
                            print(f" Näytös lisättiin viikon {viikko_nro} viikko_ohjelmaan (viikko_ohjelmat.txt), alla tiedot:")
                            print(f" Näytöksen päivämäärä: {naytos_pvm}")
                            print(f" Viikonpäivä: {naytoksen_viikonpaiva}")
                            print(f" Näytöksen tunnistenumero: {tunnistenro_naytos}")
                            print(f" Elokuvan nimi: {elokuva_naytoksessa[0].upper() + elokuva_naytoksessa[1:]}")
                            print(f" Salinumero: {naytoksen_salinro}")
                            print(f" Elokuvan näytösaika {naytoksen_aloitusaika} - {naytoksen_paattymisaika}")
                            print("")
                            print("")
                            break           
            except ValueError:
                virheilmoitus("Pitäisi antaa numerona")
                print("")
                continue
        if viikko_nro_str == "" or viikko_vuosi_str == "" or naytos_pvm_annettu == "" or elokuva_naytoksessa == "" or naytoksen_aloitusaika_annettu == "" or naytoksen_salinro_str == "":
            break              
 
def poista_viikko_ohjelmasta(viikko_nro: int, pvm, viikonpaiva, tunnistenro: int, elokuva: str, salinro: int, aloitusaika, paattymisaika) -> list:
    from datetime import datetime
    while True:
        viikko_nro_ohjelma = []
        naytosten_tunnistenrot = []
        try:
            viikko_nro_naytos_str = input("Mikä on viikon numero, minkä viikko-ohjelmasta näytös poistettaisiin? (tyhjä=poistu): ")
            if viikko_nro_naytos_str == "":
                break
            else:
                viikko_nro_naytos = int(viikko_nro_naytos_str)
                viikko_nro_tanaan = int(datetime.now().strftime('%W'))
                vuosi_tanaan = int(datetime.now().strftime('%Y'))
                while True:
                    try:
                        viikko_vuosi_str = input("ja vuosi (tyhjä=poistu): ")
                        if viikko_vuosi_str == "":
                            break
                        else:
                            viikko_vuosi = int(viikko_vuosi_str)
                            break
                    except ValueError:
                        virheilmoitus("Vuosi tulisi antaa numerona, esim. 2024")
                        print("")
                        continue
                if viikko_vuosi_str == "":
                    break              
                with open("viikko_ohjelmat.txt", "r") as viikko_ohjelmat_tiedosto: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                    for rivi in viikko_ohjelmat_tiedosto:
                        vko_ohj_osat = rivi.strip().split(", ")
                        naytosten_tunnistenrot.append(vko_ohj_osat[3])
                        if vko_ohj_osat[0] == str(viikko_nro_naytos) and vko_ohj_osat[1].split(".")[2] == str(viikko_vuosi):
                            viikko_nro_ohjelma.append(", ".join(vko_ohj_osat) + "\n")
                if viikko_nro_naytos <= 0 or viikko_nro_naytos > 53: # jos karkausvuosi, niin 53 viikkoa
                    print(" Viikkonumeron tulisi olla välillä 1-53")
                    print("")
                elif viikko_nro_naytos <= viikko_nro_tanaan and vko_ohj_osat[1].split(".")[2] == str(viikko_vuosi):
                    print(f" Tässä voitaisiin poistaa näytöksiä vain tämän viikon {viikko_nro_tanaan}/{vuosi_tanaan}) jälkeisistä viikko-ohjelmista")
                    print("")
                elif viikko_nro_ohjelma == []: # eli tyhjä
                    print(f" Viikolta {viikko_nro_naytos}/{viikko_vuosi} ohjelmassa ei ole tallentuneena näytöksiä, (ei poistettavaa), tarkistaisitko vielä viikkonumeron/vuoden")
                    print("")
                else:
                    print("")
                    print(f" Alla listana viikon {viikko_nro_naytos}/{viikko_vuosi} viikko-ohjelma ja näytöstiedot (päivämäärä, viikonpäivä, näytöksen tunnistenumero, elokuva, salinumero, aloitusaika-päättymisaika):")
                    for rivi in viikko_nro_ohjelma:
                        vko_nro_ohj_osat = rivi.strip().split(", ")
                        print(f" {vko_nro_ohj_osat[1]}, {vko_nro_ohj_osat[2]}, {vko_nro_ohj_osat[3]}, {vko_nro_ohj_osat[4][0].upper() + vko_nro_ohj_osat[4][1:]}, {vko_nro_ohj_osat[5]}, {vko_nro_ohj_osat[6]}-{vko_nro_ohj_osat[7]}")
                    print("")
                    break
        except ValueError:
            virheilmoitus("Viikkonumero pitäisi antaa numerona")
            print("")
            continue
    while True:
        viikko_ohjelmat = []
        naytostiedot_id = []
        if viikko_nro_naytos_str == "":
            break
        try: 
            tunnistenro_naytos_str = input("Mikä on yllä listalla näytöksen tunnistenumero, mikä näytös poistettaisiin (tyhjä=poistu): ")
            if tunnistenro_naytos_str == "":
                break
            else:
                tunnistenro_naytos = int(tunnistenro_naytos_str)
                if str(tunnistenro_naytos) not in naytosten_tunnistenrot:
                    print(f" Näytöksen tunnistenumeroa {tunnistenro_naytos_str} ei löydy ohjelmasta, onkohan sama kuin yllä listassa? Tallentuneena olevien näytösten tunnistenrot viikolta {viikko_nro_naytos}/{viikko_vuosi} pitäisi näkyä")
                    print("")
                else:
                    with open("viikko_ohjelmat.txt", "r") as viikko_ohjelmat_tiedosto: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                        a = viikko_ohjelmat_tiedosto.readlines()
                        for rivi in a:
                            vko_ohj_osat = rivi.strip().split(", ")
                            if vko_ohj_osat[3] == str(tunnistenro_naytos):
                                naytostiedot_id.append(", ".join(vko_ohj_osat) + "\n")
                    for i in range(len(naytostiedot_id)):
                        naytostiedot_id_osat = naytostiedot_id[i].strip().split(", ") # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                    print("")
                    print(f" Varmistus: Poistettavan näytöksen päivämäärä: {naytostiedot_id_osat[1]}, elokuva: {naytostiedot_id_osat[4][0].upper() + naytostiedot_id_osat[4][1:]}, salinumero: {naytostiedot_id_osat[5]}, aloitusaika: {naytostiedot_id_osat[6]}")
                    print("")
                    while True:
                        varmistus = input("Poistetaanko kyseinen näytös? (k/e): ")
                        with open("viikko_ohjelmat.txt", "r") as viikko_ohjelmat_tiedosto: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                            for rivi in viikko_ohjelmat_tiedosto:
                                vko_ohj_osat = rivi.strip().split(", ")
                                if varmistus == "k":
                                    if vko_ohj_osat[3] == naytostiedot_id_osat[3]:
                                        viikko_ohjelmat.append("")
                                    elif vko_ohj_osat[3] != naytostiedot_id_osat[3]:
                                        viikko_ohjelmat.append(", ".join(vko_ohj_osat) + "\n")
                                elif varmistus == "e":
                                    viikko_ohjelmat.append(", ".join(vko_ohj_osat) + "\n")
                                else:
                                    print(" Pitäisi antaa muodossa kirjain k tai e")
                                    print("")
                            break
                    with open("viikko_ohjelmat.txt", "w") as viikko_ohjelmat_tiedosto:
                        viikko_ohjelmat_tiedosto.writelines(viikko_ohjelmat)
                    with open("viikko_ohjelmat.txt", "r") as viikko_ohjelmat_tiedosto:
                        b = viikko_ohjelmat_tiedosto.readlines()
                    if len(a) > len(b):
                        print(" Okei, näytös poistettiin")
                        print("")
                        break
                    elif len(a) == len(b):
                        print(" Okei, näytöstä ei poistettu")
                        print("")
        except ValueError:
            virheilmoitus("Tunnistenumero pitäisi antaa numerona")
    
def nayta_salitiedot(salinro: int, istumapaikkalkm: int, rivimaara: int, istumapaikkoja_per_rivi: int, muita_salitietoja: str) -> list:
    print(" Alla listana saleista tiedot (salinumero, istumapaikkoja yhteensä, rivien määrä salissa, istumapaikkojen määrä rivillä, mahdollisia muita tietoja)")
    print("")
    with open("salitiedot.txt", "r") as salitiedot_tiedosto: # salinro, istumapaikkalkm, rivimaara, istumapaikkoja_per_rivi, muita_salitietoja
        for rivi in salitiedot_tiedosto:
            salitiedot_osat = rivi.strip().split(", ")
            print(f" Salinumero {salitiedot_osat[0]}: {salitiedot_osat[1]} istumapaikkaa, {salitiedot_osat[2]} riviä, {salitiedot_osat[3]} istumapaikkaa rivillä, muita tietoja: {salitiedot_osat[4]}")

def lisaa_muokkaa_salitietoja(salinro: int, istumapaikkalkm: int, rivimaara: int, istumapaikkoja_per_rivi: int, muita_salitietoja: str) -> list:
    salitiedot = []
    kaikki_salinrot = []
    # näytetään tämän hetkiset salitiedot
    with open("salitiedot.txt", "r") as salitiedot_tiedosto: # salinro, istumapaikkalkm, rivimaara, istumapaikkoja_per_rivi, muita_salitietoja
        salitiedot = salitiedot_tiedosto.readlines()
    if salitiedot != []:
        print("Alla ohjelmassa tällä hetkellä tallennettuna olevat salitiedot (salinumero, istumapaikkoja yhteensä, rivimäärä, istumapaikkoja rivillä, mahdollisia muita salitietoja):")
        for rivi in salitiedot:
            salitiedot_osat = rivi.strip().split(", ")
            print(f" Salinumero {salitiedot_osat[0]}: {salitiedot_osat[1]} istumapaikkaa, {salitiedot_osat[2]} riviä, {salitiedot_osat[3]} istumapaikkaa rivillä, muita tietoja: {salitiedot_osat[4]}")
    elif salitiedot == []:
        print(" Salitietoja ei ole vielä tallennettuna ohjelmaan, nyt tallennettaisiin ohjelmaan ensimmäisen salin tiedot")

    # lisätään salitiedot "kokonaissilmukkana"
    while True:
        while True:
            try:
                print("")
                salinro_str = input("Mikä on salin numero mikä lisätään tai minkä salitietoja muokataan? (tyhjä=poistu): ")
                if salinro_str == "":
                    break
                else:
                    salinro = int(salinro_str)
                    break
            except ValueError:
                virheilmoitus("Pitäisi antaa numerona")
                continue
        while True:
            if salinro_str == "":
                break
            else:
                try:
                    istumapaikkalkm_str = input("Mikä on salin istumapaikkalukumäärä yhteensä? (tyhjä=poistu): ")
                    if istumapaikkalkm_str == "":
                        break
                    else:
                        istumapaikkalkm = int(istumapaikkalkm_str)
                        if istumapaikkalkm < 0:
                            print(" Istumapaikkalukumääriä ei voisi merkitä alle nollan")
                        else:
                            break
                except ValueError:
                    virheilmoitus("Pitäisi antaa numerona")
                    continue
        while True:
            if salinro_str == "" or istumapaikkalkm_str == "":
                break
            else:
                try:
                    rivimaara_str = input("Kuinka monta istumaikkariviä salissa on? (tyhjä=poistu): ")
                    if rivimaara_str == "":
                        break
                    else:
                        rivimaara = int(rivimaara_str)
                        if rivimaara < 1:
                            print(" Salin rivimäärää ei voisi merkitä alle yhden")
                        else:
                            break
                except ValueError:
                    virheilmoitus("Pitäisi antaa numerona")
                    continue
        while True:
            if salinro_str == "" or istumapaikkalkm_str == "" or rivimaara_str == "":
                break
            else:
                try:
                    istumapaikkoja_rivilla_str = input("Kuinka monta istumapaikkaa on yhdessä rivissä? (tyhjä=poistu): ")
                    if istumapaikkoja_rivilla_str == "":
                        break
                    else:
                        istumapaikkoja_rivilla = int(istumapaikkoja_rivilla_str)
                        if istumapaikkoja_rivilla < 0:
                            print(" Rivin istumapaikkojen lukumäärää ei voisi merkitä alle nollan")
                        elif istumapaikkoja_rivilla * rivimaara != istumapaikkalkm: # tässä nyt näin, mutta toisaalta saattaisi olla myös niin, että eri riveillä saattaisi olla eri määrä istumapaikkoja
                            print(" ohops, ilmoitettujen istumapaikkojen lukumäärät ei täsmää, rivitiedoilla tulisi rivimaara * istumapaikkalukumäärä per rivi =", rivimaara * istumapaikkoja_rivilla)
                        else:
                            break
                except ValueError:
                    virheilmoitus("Pitäisi antaa numerona")
                    continue
        while True:
            if salinro_str == "" or istumapaikkalkm_str == "" or rivimaara_str == "" or istumapaikkoja_rivilla_str == "":
                break
            else:
                muita_salitietoja = input("Mahdollisia muita salitietoja? (tyhjä=jos ei muita tallennettavia): ")
                if muita_salitietoja == "":
                    muita_salitietoja = ".."
                for rivi in salitiedot:
                    rivi_osat = rivi.strip().split(", ")
                    kaikki_salinrot.append(rivi_osat[0])
                if str(salinro) in kaikki_salinrot:
                    for i in range(len(salitiedot)):
                        salitiedot_osat = salitiedot[i].strip().split(", ")
                        if salitiedot_osat[0] == str(salinro):
                            salitiedot[i] = f"{salinro}, {istumapaikkalkm}, {rivimaara}, {istumapaikkoja_rivilla}, {muita_salitietoja}\n"
                            print("")
                            print(f" Salin {salinro} salitiedot päivitettiin")                             
                else:
                    salitiedot.append(f"{salinro}, {istumapaikkalkm}, {rivimaara}, {istumapaikkoja_rivilla}, {muita_salitietoja}\n")                   
                with open("salitiedot.txt", "w") as salitiedot_tiedosto:
                    salitiedot_tiedosto.writelines(salitiedot)
                print("")
                print(" Alla annetut salitiedot koosteena:")
                print(f" Salinumero: {salinro}")
                print(f" Istumapaikkalukumäärä yhteensä: {istumapaikkalkm}")
                print(f" Rivejä salissa on: {rivimaara}")
                print(f" Istumapaikkoja yhdellä rivillä: {istumapaikkoja_rivilla}")
                print(f" Muita tietoja: {muita_salitietoja}")
                print(" Salitiedot tallennettiin (salitiedot.txt)")
                break
        break

def paivan_myohemmat_naytokset(pvm, elokuva: str, aloitusaika, paattymisaika, salinro: int):
    from datetime import datetime
    pvm_tanaan = datetime.now().strftime('%d.%m.%Y')
    kellonaika_nyt = datetime.now().strftime('%H:%M')
    myohemmat_naytokset = []
    viikko_ohjelmat = []
    with open("viikko_ohjelmat.txt", "r") as viikko_ohjelmat_tiedosto: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
        viikko_ohjelmat = viikko_ohjelmat_tiedosto.readlines()
        for rivi in viikko_ohjelmat:
            vko_ohj_osat = rivi.strip().split(", ")
            if vko_ohj_osat[1] == pvm_tanaan:
                if datetime.strptime(vko_ohj_osat[6], '%H:%M') >= datetime.strptime(kellonaika_nyt, '%H:%M'):
                    myohemmat_naytokset.append(", ".join(vko_ohj_osat) + "\n")
    print(f"Alla listassa tämän päivän {pvm_tanaan} myohemmät näytökset näkyvissä (mahd.) (kello on nyt {kellonaika_nyt}) (myös viikko-ohjelmassa):")
    for rivi in myohemmat_naytokset:
        print(rivi.strip())
    print("")                                    
 
def varauksen_tekeminen(varaus_pvm, varaus_elokuva: str, varaus_aloitusaika, varaus_salinro: int, varaus_istumapaikka, varaus_tunnistenro: int):
    from datetime import datetime
    while True:
        pvm_naytokset = []
        pvm_naytosten_tunnistenrot = []
        try:
            varaus_pvm_annettu = input("Mille päivämäärälle tehdään varaus? (pp.kk.vvvv) (tyhjä=poistu): ")
            if varaus_pvm_annettu == "":
                break
            else:
                varaus_pvm_annettu = varaus_pvm_annettu.strip() # poistetaan mahd. välilyönnit
                varaus_pvm_strptime = datetime.strptime(varaus_pvm_annettu, '%d.%m.%Y')
                varaus_pvm = varaus_pvm_strptime.strftime('%d.%m.%Y')
                varaus_viikkonro = varaus_pvm_strptime.strftime('%W')
                pvm_tanaan = datetime.now().strftime('%d.%m.%Y')
                kellonaika_nyt = datetime.now().strftime('%H:%M')

                if varaus_pvm_strptime < datetime.strptime(pvm_tanaan, '%d.%m.%Y'):
                    print(f" Tarkistatko päivämäärän, ei voitaisi tehdä varausmerkintää aiempiin näytöksiin, tänään olisi {pvm_tanaan}")
                    print("")
                else:
                    with open("viikko_ohjelmat.txt", "r") as viikko_ohjelmat_tiedosto: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                        viikko_ohjelmat = viikko_ohjelmat_tiedosto.readlines()
                    for rivi in viikko_ohjelmat:
                        vko_ohj_osat = rivi.strip().split(", ")
                        if vko_ohj_osat[1] == varaus_pvm:
                            pvm_naytokset.append(", ".join(vko_ohj_osat) + "\n")
                            pvm_naytosten_tunnistenrot.append(vko_ohj_osat[3])
                    print("")
                    print(f"Alla listassa {varaus_pvm} (viikko {int(varaus_viikkonro)}) pidettävät näytökset (jos tyhjä niin vielä ei olisi saatavilla)")
                    print("(viikonpäivä, näytöksen tunnistenumero, elokuva, salinumero, aloitusaika-päättymisaika):")
                    for rivi in pvm_naytokset:
                        pvm_naytokset_osat = rivi.strip().split(", ")
                        print(f" {pvm_naytokset_osat[2]}, {pvm_naytokset_osat[3]}, {pvm_naytokset_osat[4][0].upper() + pvm_naytokset_osat[4][1:]}, {pvm_naytokset_osat[5]}, {pvm_naytokset_osat[6]}-{pvm_naytokset_osat[7]}")
                    print("")    
                    break
        except ValueError:
            virheilmoitus("Päivämäärä tulisi antaa muodossa pp.kk.vvvv, esim. 20.2.2024")
            print("")
            continue

    while True:
        if varaus_pvm_annettu == "":
            break
        viikko_ohjelmat = []
        naytostiedot_id = []
        try:    
            tunnistenro_naytos_str = input("Mikä on yllä listalla näytöksen tunnistenumero, mihin tehtäisiin varaus (tyhjä=poistu): ")
            if tunnistenro_naytos_str == "":
                break
            else:
                tunnistenro_naytos = int(tunnistenro_naytos_str)
                if str(tunnistenro_naytos) not in pvm_naytosten_tunnistenrot:
                    print(f" Näytöksen tunnistenumeroa {tunnistenro_naytos_str} päivämäärällä {varaus_pvm} ei löydy ohjelmasta, tarkistatko vielä tunnistenumeron (yllä pitäisi näkyä kaikki päivän näytökset, vai olisiko toisella päivämäärällä)") 
                    print("")
                elif str(tunnistenro_naytos) in pvm_naytosten_tunnistenrot:
                    for rivi in pvm_naytokset: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                        pvm_naytokset_osat = rivi.strip().split(", ")
                        if pvm_naytokset_osat[3] == str(tunnistenro_naytos):
                            naytostiedot_id.append(", ".join(pvm_naytokset_osat) + "\n") 
                    for i in range(len(naytostiedot_id)): # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                        naytostiedot_id_osat = naytostiedot_id[i].strip().split(", ")
                        if datetime.strptime(naytostiedot_id_osat[6], '%H:%M') < datetime.strptime(kellonaika_nyt, '%H:%M') and naytostiedot_id_osat[1] == pvm_tanaan:
                            print(f" Valitettavasti näytös tunnistenumerolla {tunnistenro_naytos} on jo alkanut ja tähän näytökseen ei voida tehdä varausta (tai tarkistatko vielä tunnistenumeron) (tai mahdollisesti joku toinen näytös?)")
                            print("")
                    if datetime.strptime(naytostiedot_id_osat[6], '%H:%M') >= datetime.strptime(kellonaika_nyt, '%H:%M') and naytostiedot_id_osat[1] == pvm_tanaan:
                        break
                    elif datetime.strptime(naytostiedot_id_osat[1], '%d.%m.%Y') > datetime.strptime(pvm_tanaan, '%d.%m.%Y'): # jos myöhempänä päivänä kuin nyt
                        break
        except ValueError:
            virheilmoitus("Tunnistenumero pitäisi antaa numerona")
            print("")
            continue
                
    while True:
        while True:
            if varaus_pvm_annettu == "" or tunnistenro_naytos_str == "":
                break
            kaikki_varatut_istumapaikat = []
            varatut_istumapaikat_naytoksessa = []
            salitiedot = []
            kaikki_istumapaikat_naytoksessa = []
            vapaat_istumapaikat_naytoksessa = []
            
            with open("varatut_istumapaikat.txt", "r") as varatut_istumapaikat_tiedosto: # pvm, naytos_tunnistenro, elokuva, aloitusaika, salinro, varaus_istumapaikka (rivinro:istumapaikkanro), varaus_tunnistenro
                kaikki_varatut_istumapaikat = varatut_istumapaikat_tiedosto.readlines()
                for rivi in kaikki_varatut_istumapaikat:
                    var_istumapaikat_osat = rivi.strip().split(", ")
                    if var_istumapaikat_osat[1] == str(tunnistenro_naytos):
                        varatut_istumapaikat_naytoksessa.append(var_istumapaikat_osat[5]) # varaus_istumapaikka (rivinro:istumapaikkanro)
            with open("salitiedot.txt", "r") as salitiedot_tiedosto: # salinro, istumapaikkalkm, rivimaara, istumapaikkoja_per_rivi, muita_salitietoja
                salitiedot = salitiedot_tiedosto.readlines()
                for rivi in salitiedot:
                    salitiedot_osat = rivi.split(", ")
                    if salitiedot_osat[0] == naytostiedot_id_osat[5]: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                        for r in range(1, int(salitiedot_osat[2]) + 1):
                            for i in range(1, int(salitiedot_osat[3]) + 1):
                                kaikki_istumapaikat_naytoksessa.append(f"{r}:{i}") # rivinro:istumapaikkanro
            if varatut_istumapaikat_naytoksessa == []:
                vapaat_istumapaikat_naytoksessa = kaikki_istumapaikat_naytoksessa
            elif varatut_istumapaikat_naytoksessa != []:
                for k in range(len(kaikki_istumapaikat_naytoksessa)):
                    if kaikki_istumapaikat_naytoksessa[k] in varatut_istumapaikat_naytoksessa:
                        vapaat_istumapaikat_naytoksessa.append("v")
                    elif kaikki_istumapaikat_naytoksessa[k] not in varatut_istumapaikat_naytoksessa:
                        vapaat_istumapaikat_naytoksessa.append(kaikki_istumapaikat_naytoksessa[k])
            print(" Alla listassa vapaat istumapaikat näytöksessä (rivinro:istumapaikkanro) sekä varatut istumapaikat merkittynä kirjaimella 'v', (rivinro1 on lähimpänä näyttämöä ja istumapaikkanrot1 saliin sisääntuloa):")
            print(f" {vapaat_istumapaikat_naytoksessa}")
            print("")
            break

        while True:
            if varaus_pvm_annettu == "" or tunnistenro_naytos_str == "":
                break
            else:
                varaus_istumapaikka = input(f"Mikä istumapaikka varataan? rivinumero:istumapaikkanumero (rivinro1 on lähimpänä näyttämöä ja istumapaikkanrot1 on lähimpänä saliin sisääntuloa) (tyhjä=poistu) (ilman heittomerkkejä): ")
                if varaus_istumapaikka == "":
                    break
                elif varaus_istumapaikka not in kaikki_istumapaikat_naytoksessa: # rivinumero:istumapaikkanro
                    print(f" Tarkistatko vielä istumapaikan (vapaat istumapaikat listan ylempänä), pitäisi antaa muodossa esimerkiksi 3:3, istumapaikkaa tunnisteella {varaus_istumapaikka} ei olisi saatavilla salissa {naytostiedot_id_osat[5]}")
                    print("")
                elif varaus_istumapaikka in varatut_istumapaikat_naytoksessa: # rivinro:istumapaikkanro
                    print(" Valitettavasti paikka on jo varattu, katsoisitko vielä vapaat istumapaikat listan ylempänä (tai lista myös päivittyy aloittamalla varauksen tekemisen uudelleen)")
                    print("")
                elif varaus_istumapaikka in kaikki_istumapaikat_naytoksessa and varaus_istumapaikka not in varatut_istumapaikat_naytoksessa: 
                    kaikki_varatut_istumapaikat = []
                    with open("varatut_istumapaikat.txt", "r") as varatut_istumapaikat_tiedosto: # pvm, naytos_tunnistenro, elokuva, aloitusaika, salinro, varaus_istumapaikka (rivinumero:istumapaikkanumero), varaus_tunnistenro
                        for rivi in varatut_istumapaikat_tiedosto:
                            kaikki_varatut_istumapaikat_osat = rivi.strip().split(", ")
                            kaikki_varatut_istumapaikat.append(", ".join(kaikki_varatut_istumapaikat_osat) + "\n")
                    varaus_tunnistenro = len(kaikki_varatut_istumapaikat) + 1
                    kaikki_varatut_istumapaikat.append(f"{naytostiedot_id_osat[1]}, {naytostiedot_id_osat[3]}, {naytostiedot_id_osat[4]}, {naytostiedot_id_osat[6]}, {naytostiedot_id_osat[5]}, {varaus_istumapaikka}, {varaus_tunnistenro}\n")
                    with open("varatut_istumapaikat.txt", "w") as varatut_istumapaikat_tiedosto: # pvm, naytos_tunnistenro, elokuva, aloitusaika, salinro, varaus_istumapaikka (rivinumero:istumapaikkanumero), varaus_tunnistenro
                        varatut_istumapaikat_tiedosto.writelines(kaikki_varatut_istumapaikat)
                    print("Hyvähyvä, varaus tehtiin onnistuneesti, alla varauksen tiedot:")
                    print(f" Elokuva: {naytostiedot_id_osat[4][0].upper() + naytostiedot_id_osat[4][1:]}")   
                    print(f" Näytöksen päivämäärä ja viikonpäivä: {naytostiedot_id_osat[1]} {naytostiedot_id_osat[2]}")
                    print(f" Näytöksen aloitusaika-päättymisaika: {naytostiedot_id_osat[6]} - {naytostiedot_id_osat[7]}")
                    print(f" Näytös pidetään salissa: {naytostiedot_id_osat[5]}")
                    print(f" Varattuna istumapaikkana (rivinumero:istumapaikkanumero) on: {varaus_istumapaikka}")
                    print(f" Varauksen tunnistenumero on: {varaus_tunnistenro} (tarvittaessa varaus löytyy järjestelmästä tunnistenumerolla)")
                    print("")
                    break
        while True:
            if varaus_pvm_annettu == "" or tunnistenro_naytos_str == "" or varaus_istumapaikka == "":
                break
            else:
                lisaa_varauksia = input("Tehdäänkö samaan näytökseen vielä lisää varauksia? Jos tehdään, niin pääsee suoraan kirjaimella k (taikka tyhjä=poistu): ")
                if lisaa_varauksia == "k":
                    print("")
                    break
                elif lisaa_varauksia == "":
                    print(" Varausvahvistus tässä vielä ja Hyvää elokuvaa")
                    print("")
                    break
                else:
                    print(" Varaus on jo tehtynä, mutta painamalla enter(tyhjä) pääsee takaisin päävalikkoon taikka kirjaimella k lisäämään näytökseen mahdollisia lisävarauksia suoraan")
        if varaus_pvm_annettu == "" or tunnistenro_naytos_str == "" or varaus_istumapaikka == "" or lisaa_varauksia == "":
            break
            
def naytoksen_varaustilanne(pvm, naytos_tunnistenro: int, elokuva: str, aloitusaika, salinro: int, varaus_istumapaikka, varaus_tunnistenro: int): # Löytyy haetun näytöksen vapaat ja varatut istumapaikat
    from datetime import datetime 
    while True:                   
        pvm_naytokset = []
        pvm_naytosten_tunnistenrot = []
        try:
            pvm_naytos_annettu = input("Mikä on näytöksen päivämäärä, kun näytös pidetään (pp.kk.vvvv) (tyhjä=poistu): ")
            if pvm_naytos_annettu == "":
                break
            else:
                pvm_naytos_annettu = pvm_naytos_annettu.strip() # poistetaan mahd. välilyönnit
                pvm_naytos_strptime = datetime.strptime(pvm_naytos_annettu, '%d.%m.%Y')
                pvm_naytos = pvm_naytos_strptime.strftime('%d.%m.%Y')

                with open("viikko_ohjelmat.txt", "r") as viikko_ohjelmat_tiedosto: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                    viikko_ohjelmat = viikko_ohjelmat_tiedosto.readlines()
                    for rivi in viikko_ohjelmat:
                        vko_ohj_osat = rivi.strip().split(", ") 
                        if vko_ohj_osat[1] == pvm_naytos:
                            pvm_naytokset.append(", ".join(vko_ohj_osat) + "\n")
                            pvm_naytosten_tunnistenrot.append(vko_ohj_osat[3])
                    print("")
                    print(f"Alla listassa {pvm_naytos} pidettävät tai pidetyt näytökset (jos tyhjä niin ohjelmassa ei tallennettuja näytöksiä tai vielä ei saatavilla)")
                    print("(viikkonumero, päivämäärä, viikonpäivä, näytöksen tunnistenumero, elokuva, salinumero, aloitusaika, päättymisaika):")
                    print("")
                    for rivi in pvm_naytokset:
                        print(f" {rivi.strip()}")
                    print("")    
                    break
        except ValueError:
            virheilmoitus("Päivämäärä tulisi antaa muodossa pp.kk.vvvv, esim. 20.2.2024")
            print("")
            continue
      
    while True:
        if pvm_naytos_annettu == "":
            break
        viikko_ohjelmat = []
        naytostiedot_id = []
        try:    
            tunnistenro_naytos_str = input("Mikä on yllä listalla näytöksen tunnistenumero, minkä varaustilannetta katsottaisiin (tyhjä=poistu): ")
            if tunnistenro_naytos_str == "":
                break
            else:
                tunnistenro_naytos = int(tunnistenro_naytos_str)
                if str(tunnistenro_naytos) not in pvm_naytosten_tunnistenrot:
                    print(f" Näytöksen tunnistenumeroa {tunnistenro_naytos_str} päivämäärällä {pvm_naytos} ei löydy ohjelmasta, tarkistatko vielä tunnistenumeron (yllä pitäisi näkyä kaikki päivän näytökset, vai olisiko toisella päivämäärällä)") 
                    print("")
                elif str(tunnistenro_naytos) in pvm_naytosten_tunnistenrot:
                    for rivi in pvm_naytokset: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                        pvm_naytokset_osat = rivi.strip().split(", ")
                        if pvm_naytokset_osat[3] == str(tunnistenro_naytos):
                            naytostiedot_id.append(", ".join(pvm_naytokset_osat) + "\n") 
                    for i in range(len(naytostiedot_id)): # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                        naytostiedot_id_osat = naytostiedot_id[i].strip().split(", ")
                    break
        except ValueError:
            virheilmoitus("Tunnistenumero pitäisi antaa numerona")
            print("")
            continue
                
    while True:
        if pvm_naytos_annettu == "" or tunnistenro_naytos_str == "":
            break
        kaikki_varatut_istumapaikat = []
        varatut_istumapaikat_naytoksessa = []
        salitiedot = []
        kaikki_istumapaikat_naytoksessa = []
        vapaat_istumapaikat_naytoksessa = []
           
        with open("varatut_istumapaikat.txt", "r") as varatut_istumapaikat_tiedosto: # pvm, naytos_tunnistenro, elokuva, aloitusaika, salinro, varaus_istumapaikka (rivinro:istumapaikkanro), varaus_tunnistenro
            kaikki_varatut_istumapaikat = varatut_istumapaikat_tiedosto.readlines()
            for rivi in kaikki_varatut_istumapaikat:
                var_istumapaikat_osat = rivi.strip().split(", ")
                if var_istumapaikat_osat[1] == str(tunnistenro_naytos):
                    varatut_istumapaikat_naytoksessa.append(var_istumapaikat_osat[5]) # varaus_istumapaikka (rivinro:istumapaikkanro)
        with open("salitiedot.txt", "r") as salitiedot_tiedosto: # salinro, istumapaikkalkm, rivimaara, istumapaikkoja_per_rivi, muita_salitietoja
            salitiedot = salitiedot_tiedosto.readlines()
            for rivi in salitiedot:
                salitiedot_osat = rivi.split(", ")
                if salitiedot_osat[0] == naytostiedot_id_osat[5]: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                    for r in range(1, int(salitiedot_osat[2]) + 1):
                        for i in range(1, int(salitiedot_osat[3]) + 1):
                            kaikki_istumapaikat_naytoksessa.append(f"{r}:{i}") # rivinro:istumapaikkanro
        if varatut_istumapaikat_naytoksessa == []:
            vapaat_istumapaikat_naytoksessa = kaikki_istumapaikat_naytoksessa
        elif varatut_istumapaikat_naytoksessa != []:
            for k in range(len(kaikki_istumapaikat_naytoksessa)):
                if kaikki_istumapaikat_naytoksessa[k] in varatut_istumapaikat_naytoksessa:
                    vapaat_istumapaikat_naytoksessa.append("v")
                elif kaikki_istumapaikat_naytoksessa[k] not in varatut_istumapaikat_naytoksessa:
                    vapaat_istumapaikat_naytoksessa.append(kaikki_istumapaikat_naytoksessa[k])
        print(" Alla listassa vapaat istumapaikat näytöksessä (rivinro:istumapaikkanro) sekä varatut istumapaikat merkittynä kirjaimella 'v', (rivinro1 on lähimpänä näyttämöä ja istumapaikkanrot1 saliin sisääntuloa):")
        print(f" {vapaat_istumapaikat_naytoksessa}")
        print("")
        break    
    
def varaustiedot_tunnistenro(): # naytos_pvm, viikonpäivä, näytöksen tunnistenro, elokuva, aloitusaika, päättymisaika, salinumero, varattu istumapaikka (rivinro:istumapaikkanro), varauksen tunnistenro
    from datetime import datetime
    while True:
        varaustietoja = []
        varaustiedot_tunnistenro = []
        try:    
            varaus_tunnistenro_str = input("Minkä varauksen varaustiedot näytetään? Varauksen tunnistenumero (tyhjä=poistu): ")
            if varaus_tunnistenro_str == "":
                break
            else:
                varaus_tunnistenro = int(varaus_tunnistenro_str)
                with open("varatut_istumapaikat.txt", "r") as varatut_istumapaikat_tiedosto: # pvm, naytos_tunnistenro, elokuva, aloitusaika, salinro, varaus_istumapaikka (rivinro:istumapaikkanro), varaus_tunnistenro
                    for rivi in varatut_istumapaikat_tiedosto:
                        var_istumapaikat_osat = rivi.strip().split(", ")
                        if var_istumapaikat_osat[6] == str(varaus_tunnistenro):
                            varaustietoja.append(", ".join(var_istumapaikat_osat)) 
                for i in range(len(varaustietoja)):
                    varaustietoja_osat = varaustietoja[i].strip().split(", ")
                if varaustietoja == []:
                    print(f"Varausta tunnistenumerolla {varaus_tunnistenro} ei löytynyt ohjelmasta, tarkistatko vielä tunnistenumeron, pitäisikö varmasti olla tallentuneena?")
                    print("")
                elif varaustietoja != []:
                    with open("viikko_ohjelmat.txt", "r") as viikko_ohjelmat_tiedosto: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                        for rivi in viikko_ohjelmat_tiedosto:
                            vko_ohj_osat = rivi.strip().split(", ") 
                            if vko_ohj_osat[3] == varaustietoja_osat[1]:
                                varaustiedot_tunnistenro.append(f"{varaustietoja_osat[0]}, {vko_ohj_osat[2]}, {varaustietoja_osat[1]}, {varaustietoja_osat[2]}, {varaustietoja_osat[3]}, {vko_ohj_osat[7]}, {varaustietoja_osat[4]}, {varaustietoja_osat[5]}, {varaustietoja_osat[6]}\n")
                    print("")
                    print(f"Alla varauksen tiedot tunnistenumerolla {varaus_tunnistenro} (pvm, viikonpäivä, näytöksen tunnistenro, elokuva, aloitusaika-päättymisaika, salinumero, varattu istumapaikka (rivinro:istumapaikkanro)")
                    print("")
                    for rivi in varaustiedot_tunnistenro:
                        rivi_osat = rivi.strip().split(", ")
                        print(f" {rivi_osat[0]}, {rivi_osat[1]}, {rivi_osat[2]}, {rivi_osat[3]}, {rivi_osat[4]}-{rivi_osat[5]}, {rivi_osat[6]}, {rivi_osat[7]}")
                    break
        except ValueError:
            virheilmoitus("Tunnistenumero pitäisi antaa numerona")
            print("")
            continue

def paivan_varaustilanne(): # pvm, viikonpaiva, naytos_tunnistenro, aloitusaika-päättymisaika, salinro, varauslukumaara /maksimimäärä, elokuva
    from datetime import datetime
    while True:
        viikko_ohjelmat = []
        salitiedot = []
        kaikki_varatut_istumapaikat = []
        paivan_naytokset_varausten_naytostunnistenrot = []
        naytostunnistenro_varauslukumaarat_skirja = {}
        naytokset_varauslkm = []
        paivan_naytokset_varausmaarat_lista = []
        try:
            pvm_annettu = input("Minkä päivän varaustilanne näytetään? (pp.kk.vvvv) (tyhjä=poistu): ")
            if pvm_annettu == "":
                break
            else:
                pvm_annettu = pvm_annettu.strip() # poistetaan mahd. välilyönnit
                pvm_strptime = datetime.strptime(pvm_annettu, '%d.%m.%Y')
                pvm = pvm_strptime.strftime('%d.%m.%Y')

                with open("viikko_ohjelmat.txt", "r") as viikko_ohjelmat_tiedosto: # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                    viikko_ohjelmat = viikko_ohjelmat_tiedosto.readlines()     
                with open("salitiedot.txt", "r") as salitiedot_tiedosto: # salinro, istumapaikkalkm, rivimaara, istumapaikkoja_per_rivi, muita_salitietoja
                    salitiedot = salitiedot_tiedosto.readlines()
                with open("varatut_istumapaikat.txt", "r") as varatut_istumapaikat_tiedosto: # pvm, naytos_tunnistenro, elokuva, aloitusaika, salinro, varaus_istumapaikka (rivinro:istumapaikkanro), varaus_tunnistenro
                    kaikki_varatut_istumapaikat = varatut_istumapaikat_tiedosto.readlines()
    
                # muodostetaan lista, mihin on lisätty jokaiseen eri varaustunnistenumero-riviin sisältyvä näytöstunnistenumero -> tällöin samojen näytöstunnistenumeroiden rivisumma = näytökseen (näytöstunnistenumero) tehtyjen varausten lukumäärä
                for rivi in kaikki_varatut_istumapaikat:
                    var_istumapaikat_osat = rivi.strip().split(", ")
                    if var_istumapaikat_osat[0] == pvm:
                        paivan_naytokset_varausten_naytostunnistenrot.append(var_istumapaikat_osat[1])
            
                # muodostetaan sanakirja, jossa avaimena näytöstunnistenumero sekä arvona näytökseen tehty varauslukumäärä
                for naytos_tunnistenro in paivan_naytokset_varausten_naytostunnistenrot:
                    if naytos_tunnistenro in naytostunnistenro_varauslukumaarat_skirja:
                        naytostunnistenro_varauslukumaarat_skirja[naytos_tunnistenro] += 1
                    else:
                        naytostunnistenro_varauslukumaarat_skirja[naytos_tunnistenro] = 1
    
                # muodostetaan sanakirjan tiedoista lista, missä näytöstunnukset (mihin tehty varauksia) ja näihin tehdyt varauslukumäärät
                for naytos_tunnistenro, varauslukumaarat in naytostunnistenro_varauslukumaarat_skirja.items():
                    naytokset_varauslkm.append(f"{naytos_tunnistenro}, {varauslukumaarat}\n")
    
                # muodostetaan lista, missä päivän kaikki näytökset, näytös- ja salitietoja sekä tehtyjen varausten tämän hetkiset varausmäärät
                # pvm, viikonpaiva, naytos_tunnistenro, aloitusaika-päättymisaika, salinro, varauslukumaara /maksimimäärä, elokuva
                for i in range(len(viikko_ohjelmat)): 
                    vko_ohj_osat = viikko_ohjelmat[i].strip().split(", ") # viikko_nro, pvm, viikonpaiva, tunnistenro, elokuva, salinro, aloitusaika, paattymisaika
                    for j in range(len(salitiedot)):
                        salitiedot_osat = salitiedot[j].strip().split(", ") # salinro, istumapaikkalkm, rivimaara, istumapaikkoja_per_rivi, muita_salitietoja
                        for k in range(len(kaikki_varatut_istumapaikat)):
                            var_istumapaikat_osat = kaikki_varatut_istumapaikat[k].strip().split(", ") # pvm, naytos_tunnistenro, elokuva, aloitusaika, salinro, varaus_istumapaikka (rivinro:istumapaikkanro), varaus_tunnistenro
                            for l in range(len(naytokset_varauslkm)): 
                                naytokset_varauslkm_osat = naytokset_varauslkm[l].strip().split(", ") # naytos_tunnistenro, varausten lukumaara
                                if vko_ohj_osat[1] == pvm and vko_ohj_osat[5] == salitiedot_osat[0]:
                                    if vko_ohj_osat[3] == var_istumapaikat_osat[1] == naytokset_varauslkm_osat[0]:
                                        if f"{vko_ohj_osat[1]}, {vko_ohj_osat[2]}, {vko_ohj_osat[3]}, {vko_ohj_osat[6]}-{vko_ohj_osat[7]}, {vko_ohj_osat[5]}, {naytokset_varauslkm_osat[1]}/{salitiedot_osat[1]}, {vko_ohj_osat[4]}\n" not in paivan_naytokset_varausmaarat_lista:
                                            # pvm, viikonpaiva, naytos_tunnistenro, aloitusaika-päättymisaika, salinro, varauslukumaara /maksimimäärä, elokuva   
                                            paivan_naytokset_varausmaarat_lista.append(f"{vko_ohj_osat[1]}, {vko_ohj_osat[2]}, {vko_ohj_osat[3]}, {vko_ohj_osat[6]}-{vko_ohj_osat[7]}, {vko_ohj_osat[5]}, {naytokset_varauslkm_osat[1]}/{salitiedot_osat[1]}, {vko_ohj_osat[4]}\n")
                                    elif vko_ohj_osat[3] not in paivan_naytokset_varausten_naytostunnistenrot:
                                        if f"{vko_ohj_osat[1]}, {vko_ohj_osat[2]}, {vko_ohj_osat[3]}, {vko_ohj_osat[6]}-{vko_ohj_osat[7]}, {vko_ohj_osat[5]}, {0}/{salitiedot_osat[1]}, {vko_ohj_osat[4]}\n" not in paivan_naytokset_varausmaarat_lista:
                                            # lisätään listaan myös näytökset, mihin jos ei oltaisi tehty vielä varauksia ja varauslukumäärä näin arvolla 0
                                            paivan_naytokset_varausmaarat_lista.append(f"{vko_ohj_osat[1]}, {vko_ohj_osat[2]}, {vko_ohj_osat[3]}, {vko_ohj_osat[6]}-{vko_ohj_osat[7]}, {vko_ohj_osat[5]}, {0}/{salitiedot_osat[1]}, {vko_ohj_osat[4]}\n")
                print("")
                print(f"Alla ohjelmassa olevat päivän {pvm} näytös- ja varaustiedot koontina (pvm, viikonpäivä, näytöksen tunnistenro, aloitusaika-päättymisaika, salinumero, varauslukumäärä/maksimimäärä, elokuva):") 
                print("")
                for rivi in paivan_naytokset_varausmaarat_lista:
                    print(f" {rivi.strip()}")
                print("")
                break                        

        except ValueError:
            virheilmoitus("Päivämäärä tulisi antaa muodossa pp.kk.vvvv, esim. 20.2.2024")
            print("")
            continue                             

def varaustiedot_mahd_muokkaa():
    varaustiedot_tunnistenro() # naytos_pvm, viikonpäivä, näytöksen tunnistenro, elokuva, aloitusaika, päättymisaika, salinumero, varattu istumapaikka (rivinro:istumapaikkanro), varauksen tunnistenro
    print("")
    print("(varaukseen muutosten tekemistä ei tässä vielä saatavilla)")
    print("")
    """Voisi jatkaa ohjelmaa"""

def hae_elokuvan_aiempia_varauslkm():
    """Voisi jatkaa ohjelmaa, elokuvan varauslukumäärien/katsojamäärien haku joillakin vaihtoedoilla"""

def palautteen_antaminen():
    """Voisi jatkaa ohjelmaa"""

def palautteet():
    """Voisi jatkaa ohjelmaa ja tehdä esim. kysymysrungon, mihin asiakkaat voisivat sitten täyttää ja mitkä tulisivat tällä funktiolla sitten näkyviin 
    esim. haetulta aikaväliltä, edellisen viikon osalta tai muutaman edellisen päivän osalta"""

def anna_elokuva_arvostelu():
    """Voisi jatkaa ohjelmaa"""

def lisaa_ideoita():
    """Voisi mahdollisesti jatkaa ohjelmaa ja lisätä "ideapankki"-lisäysmahdollisuuden"""


# mahd. virhetilanteiden käsittelyistä
def virheilmoitus(viesti):
    print(f" {viesti}")

while True:
    a_vai_y = input("Asiakas-käyttöliittymä vai ylläpitäjä-käyttöliittymä? (a/y): ") # tässä näin, mutta oikeasti voisi olla esim. sisäänkirjautumisen kautta
    if a_vai_y == "y":
        print("")
        mainvalikko_yllapitaja()
        break
    elif a_vai_y == "a":
        print("")
        mainvalikko_asiakkaalle()
        break
    else:
        print(" Asiakas-käyttöliittymään pääsee a-kirjaimella ja ylläpitäjä-käyttöliittymään y-kirjaimella")
        print("")
